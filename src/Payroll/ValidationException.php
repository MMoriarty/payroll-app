<?php

declare(strict_types=1);

namespace Payroll;

class ValidationException extends PayrollException
{
    public static function create(string $message, \Throwable $throwable = null): self
    {
        return new self($message, 0, $throwable);
    }
}
