<?php

declare(strict_types=1);

namespace Payroll\Employee;

interface EmployeePersister
{
    public function create(Employee $employee): void;
}
