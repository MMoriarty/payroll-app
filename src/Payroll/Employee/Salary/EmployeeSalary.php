<?php

declare(strict_types=1);

namespace Payroll\Employee\Salary;

use Payroll\Employee\Employee;

class EmployeeSalary
{
    public function __construct(
        public readonly Employee $employee,
        public readonly int $salaryAddition,
        public readonly int $totalSalary,
    ) {}
}
