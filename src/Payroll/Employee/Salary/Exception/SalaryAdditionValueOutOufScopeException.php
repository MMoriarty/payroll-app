<?php

declare(strict_types=1);

namespace Payroll\Employee\Salary\Exception;

class SalaryAdditionValueOutOufScopeException extends SalaryException
{
    public static function create(string $message): self
    {
        return new self($message);
    }
}
