<?php

declare(strict_types=1);

namespace Payroll\Employee\Salary\Exception;

use Payroll\Employee\Salary\SalaryAddition\SalaryAddition;
use Payroll\Employee\Salary\SalaryCalculator\EmployeeSalaryCalculator;

class SalaryCalculationModeNotSupportedException extends SalaryException
{
    public static function specific(
        SalaryAddition $salaryAddition,
        EmployeeSalaryCalculator $employeeSalaryCalculator
    ): self {
        return new self(
            \sprintf(
                'Calculator %s does not support "%s" salary addition type.',
                \get_class($employeeSalaryCalculator),
                $salaryAddition->type->name()
            )
        );
    }

    public static function any(SalaryAddition $salaryAddition): self
    {
        return new self(
            \sprintf(
                'Failed to found calculator that supports "%s" salary addition type.',
                $salaryAddition->type->name()
            )
        );
    }
}
