<?php

declare(strict_types=1);

namespace Payroll\Employee\Salary\Exception;

class SalaryAdditionValueNotSupportedException extends SalaryException
{
    public static function create(string $value): self
    {
        return new self(\sprintf('Salary addition value "%s" is not supported.', $value));
    }
}
