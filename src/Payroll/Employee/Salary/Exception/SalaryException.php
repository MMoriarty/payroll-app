<?php

declare(strict_types=1);

namespace Payroll\Employee\Salary\Exception;

use Payroll\Employee\Exception\EmployeeException;

abstract class SalaryException extends EmployeeException
{
}
