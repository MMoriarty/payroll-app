<?php

declare(strict_types=1);

namespace Payroll\Employee\Salary\SalaryReport;

use Payroll\Employee\Employee;

class SalaryReport
{
    public function __construct(
        public readonly Employee $employee,
        public readonly string $formattedBaseSalary,
        public readonly int $totalSalaryAddition,
        public readonly string $formattedTotalSalaryAddition,
        public readonly int $finalSalary,
        public readonly string $formattedFinalSalary,
    ) {}
}
