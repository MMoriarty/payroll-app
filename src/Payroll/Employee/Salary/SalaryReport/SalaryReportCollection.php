<?php

declare(strict_types=1);

namespace Payroll\Employee\Salary\SalaryReport;

use Payroll\Employee\Sorting\SortBy;
use Payroll\Employee\Sorting\SortOrder;

class SalaryReportCollection implements \IteratorAggregate, \Countable
{
    /**
     * @var SalaryReport[]
     */
    protected array $reports = [];

    public function add(SalaryReport $salaryReport): void
    {
        $this->reports[] = $salaryReport;
    }

    public function sort(SortBy $sort, SortOrder $order = SortOrder::ASC): void
    {
        switch ($sort) {
            case SortBy::FINAL_SALARY:
                usort(
                    $this->reports,
                    static fn(SalaryReport $x, SalaryReport $y) => $x->finalSalary <=> $y->finalSalary
                );
                break;
            case SortBy::TOTAL_SALARY_ADDITION:
                usort(
                    $this->reports,
                    static fn(SalaryReport $x, SalaryReport $y) => $x->totalSalaryAddition <=> $y->totalSalaryAddition
                );
                break;
            default:
                throw new \InvalidArgumentException(
                    \sprintf(
                        'Collection can be sorted only by %s and %s.',
                        SortBy::FINAL_SALARY->value,
                        SortBy::TOTAL_SALARY_ADDITION->value
                    )
                );
        }

        if ($order === SortOrder::DESC) {
            $this->reports = \array_reverse($this->reports);
        }
    }

    /**
     * @return \Traversable|SalaryReport[]
     */
    public function getIterator(): \Traversable
    {
        return new \ArrayIterator($this->reports);
    }

    public function count(): int
    {
        return \count($this->reports);
    }
}
