<?php

declare(strict_types=1);

namespace Payroll\Employee\Salary\SalaryAddition;

class SalaryAddition
{
    public function __construct(
        public readonly SalaryAdditionType $type,
        public readonly int $value
    ) {}
}
