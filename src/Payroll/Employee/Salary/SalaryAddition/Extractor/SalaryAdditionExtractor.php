<?php

declare(strict_types=1);

namespace Payroll\Employee\Salary\SalaryAddition\Extractor;

use Payroll\Employee\Salary\Exception\SalaryAdditionValueNotSupportedException;
use Payroll\Employee\Salary\Exception\SalaryAdditionValueOutOufScopeException;
use Payroll\Employee\Salary\SalaryAddition\SalaryAddition;

interface SalaryAdditionExtractor
{
    /**
     * @throws SalaryAdditionValueNotSupportedException
     * @throws SalaryAdditionValueOutOufScopeException
     */
    public function extractFrom(string $value): SalaryAddition;

    public function supports(string $value): bool;
}
