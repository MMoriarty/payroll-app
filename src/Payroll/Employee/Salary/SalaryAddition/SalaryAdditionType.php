<?php

declare(strict_types=1);

namespace Payroll\Employee\Salary\SalaryAddition;

enum SalaryAdditionType: int
{
    case FIXED = 1;
    case PERCENTAGE = 2;

    public function name(): string
    {
        return ucfirst(match ($this) {
            self::FIXED => 'fixed',
            self::PERCENTAGE => 'percentage',
        });
    }
}
