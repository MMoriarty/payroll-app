<?php

declare(strict_types=1);

namespace Payroll\Employee\Salary;

interface SalaryFormatter
{
    public function format(int $value): string;
}
