<?php

declare(strict_types=1);

namespace Payroll\Employee\Salary\SalaryCalculator;

use Payroll\Employee\Employee;
use Payroll\Employee\Salary\EmployeeSalary;
use Payroll\Employee\Salary\Exception\SalaryCalculationModeNotSupportedException;

interface EmployeeSalaryCalculator
{
    /**
     * @throws SalaryCalculationModeNotSupportedException
     */
    public function calculateForEmployee(Employee $employee): EmployeeSalary;

    public function supports(Employee $employee): bool;
}
