<?php

declare(strict_types=1);

namespace Payroll\Employee\Salary;

use Payroll\ValidationException;

interface SalaryParser
{
    /**
     * @throws ValidationException
     */
    public function parse(string $value): int;
}
