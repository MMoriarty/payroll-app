<?php

declare(strict_types=1);

namespace Payroll\Employee;

use Payroll\ValidationException;

interface EmployeeValidator
{
    /**
     * @throws ValidationException
     */
    public function validate(Employee $employee): void;
}
