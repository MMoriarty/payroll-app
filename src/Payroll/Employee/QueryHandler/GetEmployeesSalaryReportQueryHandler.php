<?php

declare(strict_types=1);

namespace Payroll\Employee\QueryHandler;

use Payroll\Department\DepartmentRepository;
use Payroll\Department\Exception\DepartmentNotFoundException;
use Payroll\Employee\Exception\UnrecognizedSortingOptionException;
use Payroll\Employee\Filter\FilterBy;
use Payroll\Employee\Query\GetEmployeesSalaryReportQuery;
use Payroll\Employee\EmployeeRepository;
use Payroll\Employee\Salary\Exception\SalaryCalculationModeNotSupportedException;
use Payroll\Employee\Salary\SalaryCalculator\EmployeeSalaryCalculator;
use Payroll\Employee\Salary\SalaryFormatter;
use Payroll\Employee\Salary\SalaryReport\SalaryReport;
use Payroll\Employee\Salary\SalaryReport\SalaryReportCollection;
use Payroll\Employee\Sorting\SortBy;
use Payroll\Employee\Sorting\SortOrder;

class GetEmployeesSalaryReportQueryHandler
{
    protected EmployeeRepository $employeeRepository;
    protected SalaryFormatter $salaryFormatter;
    protected EmployeeSalaryCalculator $employeeSalaryCalculator;
    protected DepartmentRepository $departmentRepository;

    public function __construct(
        EmployeeRepository $employeeRepository,
        SalaryFormatter $salaryFormatter,
        EmployeeSalaryCalculator $employeeSalaryCalculator,
        DepartmentRepository $departmentRepository
    ) {
        $this->employeeRepository = $employeeRepository;
        $this->salaryFormatter = $salaryFormatter;
        $this->employeeSalaryCalculator = $employeeSalaryCalculator;
        $this->departmentRepository = $departmentRepository;
    }

    /**
     * @throws SalaryCalculationModeNotSupportedException
     * @throws UnrecognizedSortingOptionException
     */
    public function handle(GetEmployeesSalaryReportQuery $query): SalaryReportCollection
    {
        $reports = new SalaryReportCollection();
        $sort = $this->getSortingFromQuery($query);
        $sortOrder = $query->sortingOrder ? SortOrder::from($query->sortingOrder) : SortOrder::ASC;

        try {
            $employees = $this->employeeRepository->findMany(
                $this->getFilterFromQuery($query),
                $sort,
                $sortOrder,
            );
        } catch (DepartmentNotFoundException) {
            return $reports;
        }

        foreach ($employees as $employee) {
            $employeeSalary = $this->employeeSalaryCalculator->calculateForEmployee($employee);

            $reports->add(
                new SalaryReport(
                    $employee,
                    $this->salaryFormatter->format($employee->salary),
                    $employeeSalary->salaryAddition,
                    $this->salaryFormatter->format($employeeSalary->salaryAddition),
                    $employeeSalary->totalSalary,
                    $this->salaryFormatter->format($employeeSalary->totalSalary),
                )
            );
        }

        $this->applyCollectionSorting($reports, $sort, $sortOrder);

        return $reports;
    }

    /**
     * @throws DepartmentNotFoundException
     */
    private function getFilterFromQuery(GetEmployeesSalaryReportQuery $query): ?FilterBy
    {
        if ($query->filterDepartmentName !== null) {
            $department = $this->departmentRepository->findByName($query->filterDepartmentName);
            if ($department === null) {
                throw DepartmentNotFoundException::byName($query->filterDepartmentName);
            }
        }

        return new FilterBy($query->filterFirstname, $query->filterLastname, $department ?? null);
    }

    /**
     * @throws UnrecognizedSortingOptionException
     */
    private function getSortingFromQuery(GetEmployeesSalaryReportQuery $query): ?SortBy
    {
        if ($query->sortingField === null) {
            return null;
        }

        $sort = SortBy::tryFrom($query->sortingField);
        if (!$sort) {
            throw UnrecognizedSortingOptionException::create($query->sortingField);
        }

        return $sort;
    }

    private function applyCollectionSorting(SalaryReportCollection $collection, ?SortBy $sort, SortOrder $order): void
    {
        if ($sort !== SortBy::TOTAL_SALARY_ADDITION && $sort !== SortBy::FINAL_SALARY) {
            return;
        }

        $collection->sort($sort, $order);
    }
}
