<?php

declare(strict_types=1);

namespace Payroll\Employee\Filter;

use Payroll\Department\Department;

class FilterBy
{
    public function __construct(
        public readonly ?string $firstName = null,
        public readonly ?string $lastName = null,
        public readonly ?Department $department = null
    ) {}
}
