<?php

declare(strict_types=1);

namespace Payroll\Employee\Exception;

use Payroll\PayrollException;

abstract class EmployeeException extends PayrollException
{
}
