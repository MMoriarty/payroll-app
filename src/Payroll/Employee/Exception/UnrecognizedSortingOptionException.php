<?php

declare(strict_types=1);

namespace Payroll\Employee\Exception;

class UnrecognizedSortingOptionException extends EmployeeException
{
    public static function create(string $sortingOption): self
    {
        return new self(\sprintf('Sorting employees by "%s" is not supported.', $sortingOption));
    }
}
