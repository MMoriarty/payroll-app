<?php

declare(strict_types=1);

namespace Payroll\Employee\Sorting;

enum SortOrder: string
{
    case ASC = 'ASC';
    case DESC = 'DESC';
}
