<?php

declare(strict_types=1);

namespace Payroll\Employee\Sorting;

enum SortBy: string
{
    case FIRST_NAME = 'First name';
    case LAST_NAME = 'Last name';
    case DEPARTMENT = 'Department';
    case BASE_SALARY = 'Base salary';
    case TOTAL_SALARY_ADDITION = 'Salary addition';
    case SALARY_ADDITION_TYPE = 'Addition type';
    case FINAL_SALARY = 'Total salary';

    /**
     * @return string[]
     */
    public static function options(): array
    {
        return \array_map(static fn (\BackedEnum $item) => $item->value, SortBy::cases());
    }
}
