<?php

declare(strict_types=1);

namespace Payroll\Employee;

use Payroll\Employee\Filter\FilterBy;
use Payroll\Employee\Sorting\SortBy;
use Payroll\Employee\Sorting\SortOrder;

interface EmployeeRepository
{
    /**
     * @return Employee[]
     */
    public function findMany(
        FilterBy $filter = null,
        SortBy $sortBy = null,
        SortOrder $sortOrder = SortOrder::ASC
    ): array;
}
