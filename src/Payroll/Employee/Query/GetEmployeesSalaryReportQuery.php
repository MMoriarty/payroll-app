<?php

declare(strict_types=1);

namespace Payroll\Employee\Query;

class GetEmployeesSalaryReportQuery
{
    public function __construct(
        public readonly ?string $filterFirstname = null,
        public readonly ?string $filterLastname = null,
        public readonly ?string $filterDepartmentName = null,
        public readonly ?string $sortingField = null,
        public readonly ?string $sortingOrder = null,
    ) {}
}
