<?php

declare(strict_types=1);

namespace Payroll\Employee\Command;

class CreateEmployeeCommand
{
    public function __construct(
        public readonly string $firstName,
        public readonly string $lastName,
        public readonly string $departmentName,
        public readonly string $salary,
        public readonly ?string $employmentDate = null,
    ) {}
}
