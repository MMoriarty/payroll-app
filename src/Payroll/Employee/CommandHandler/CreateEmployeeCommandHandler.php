<?php

declare(strict_types=1);

namespace Payroll\Employee\CommandHandler;

use App\Clock\Clock;
use Payroll\Department\Exception\DepartmentNotFoundException;
use Payroll\Department\DepartmentRepository;
use Payroll\Employee\Command\CreateEmployeeCommand;
use Payroll\Employee\Employee;
use Payroll\Employee\EmployeePersister;
use Payroll\Employee\Salary\SalaryParser;
use Payroll\Employee\EmployeeValidator;
use Payroll\ValidationException;

class CreateEmployeeCommandHandler
{
    protected EmployeePersister $employeePersister;
    protected SalaryParser $salaryParser;
    protected DepartmentRepository $departmentRepository;
    protected Clock $clock;
    protected EmployeeValidator $validator;

    public function __construct(
        EmployeePersister $employeePersister,
        SalaryParser $salaryParser,
        DepartmentRepository $departmentRepository,
        Clock $clock,
        EmployeeValidator $validator
    ) {
        $this->employeePersister = $employeePersister;
        $this->salaryParser = $salaryParser;
        $this->departmentRepository = $departmentRepository;
        $this->clock = $clock;
        $this->validator = $validator;
    }

    /**
     * @throws DepartmentNotFoundException
     * @throws ValidationException
     */
    public function __invoke(CreateEmployeeCommand $command): void
    {
        $employee = $this->createEmployeeFromCommand($command);
        $this->validateEmployeeData($employee);

        $this->employeePersister->create($employee);
    }

    /**
     * @throws DepartmentNotFoundException
     * @throws ValidationException
     */
    private function createEmployeeFromCommand(CreateEmployeeCommand $command): Employee
    {
        $department = $this->departmentRepository->findByName($command->departmentName);
        if ($department === null) {
            throw DepartmentNotFoundException::byName($command->departmentName);
        }

        return new Employee(
            $command->firstName,
            $command->lastName,
            $this->salaryParser->parse($command->salary),
            $department,
            ($command->employmentDate === null ? $this->clock->now() : $this->clock->createFromFormat(
                'Y-m-d',
                $command->employmentDate
            ))
        );
    }

    /**
     * @throws ValidationException
     */
    private function validateEmployeeData(Employee $employee): void
    {
        $this->validator->validate($employee);
    }
}
