<?php

declare(strict_types=1);

namespace Payroll\Employee;

use Payroll\Department\Department;

class Employee
{
    public function __construct(
        public readonly string $firstName,
        public readonly string $lastName,
        public readonly int $salary,
        public readonly Department $department,
        public readonly ?\DateTime $employmentDate,
        public readonly ?string $id = null,
    ) {}
}
