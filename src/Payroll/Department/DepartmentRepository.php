<?php

declare(strict_types=1);

namespace Payroll\Department;

interface DepartmentRepository
{
    public function find(string $id): ?Department;
    public function findByName(string $name): ?Department;
}
