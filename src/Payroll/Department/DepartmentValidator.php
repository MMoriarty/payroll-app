<?php

declare(strict_types=1);

namespace Payroll\Department;

use Payroll\ValidationException;

interface DepartmentValidator
{
    /**
     * @throws ValidationException
     */
    public function validate(Department $department): void;
}
