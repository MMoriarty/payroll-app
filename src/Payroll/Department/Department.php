<?php

declare(strict_types=1);

namespace Payroll\Department;

use Payroll\Employee\Salary\SalaryAddition\SalaryAddition;

class Department
{
    public function __construct(
        public readonly string $name,
        public readonly SalaryAddition $salaryAddition,
        public readonly ?string $id = null,
    ) {}
}
