<?php

declare(strict_types=1);

namespace Payroll\Department\Exception;

class DepartmentNotFoundException extends DepartmentException
{
    public static function byName(string $name): self
    {
        return new self(\sprintf('Department with name "%s" does not exists.', $name));
    }
}
