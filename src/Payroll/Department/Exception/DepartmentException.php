<?php

declare(strict_types=1);

namespace Payroll\Department\Exception;

use Payroll\PayrollException;

abstract class DepartmentException extends PayrollException
{
}
