<?php

declare(strict_types=1);

namespace Payroll\Department\Exception;

class DuplicatedDepartmentNameException extends DepartmentException
{
    public static function create(string $name): self
    {
        return new self(\sprintf('Department with name "%s" already exists.', $name));
    }
}
