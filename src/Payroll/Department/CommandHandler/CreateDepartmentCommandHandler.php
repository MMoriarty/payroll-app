<?php

declare(strict_types=1);

namespace Payroll\Department\CommandHandler;

use Payroll\Department\Command\CreateDepartmentCommand;
use Payroll\Department\Department;
use Payroll\Department\Exception\DuplicatedDepartmentNameException;
use Payroll\Department\DepartmentPersister;
use Payroll\Department\DepartmentRepository;
use Payroll\Department\DepartmentValidator;
use Payroll\Employee\Salary\Exception\SalaryAdditionValueNotSupportedException;
use Payroll\Employee\Salary\Exception\SalaryAdditionValueOutOufScopeException;
use Payroll\Employee\Salary\SalaryAddition\Extractor\SalaryAdditionExtractor;
use Payroll\ValidationException;

class CreateDepartmentCommandHandler
{
    protected DepartmentRepository $departmentRepository;
    protected DepartmentPersister $departmentPersister;
    protected SalaryAdditionExtractor $salaryAdditionExtractor;
    protected DepartmentValidator $validator;

    public function __construct(
        DepartmentRepository $departmentRepository,
        DepartmentPersister $departmentPersister,
        SalaryAdditionExtractor $salaryAdditionExtractor,
        DepartmentValidator $validator
    ) {
        $this->departmentRepository = $departmentRepository;
        $this->departmentPersister = $departmentPersister;
        $this->salaryAdditionExtractor = $salaryAdditionExtractor;
        $this->validator = $validator;
    }

    /**
     * @throws DuplicatedDepartmentNameException
     * @throws SalaryAdditionValueNotSupportedException
     * @throws SalaryAdditionValueOutOufScopeException
     * @throws ValidationException
     */
    public function __invoke(CreateDepartmentCommand $command): void
    {
        if ($this->departmentRepository->findByName($command->departmentName) !== null) {
            throw DuplicatedDepartmentNameException::create($command->departmentName);
        }

        $department = $this->createDepartmentFromCommand($command);
        $this->validateDepartment($department);

        $this->departmentPersister->create($department);
    }

    /**
     * @throws SalaryAdditionValueNotSupportedException
     * @throws SalaryAdditionValueOutOufScopeException
     */
    private function createDepartmentFromCommand(CreateDepartmentCommand $command): Department
    {
        return new Department(
            $command->departmentName,
            $this->salaryAdditionExtractor->extractFrom($command->salaryAddition),
        );
    }

    /**
     * @throws ValidationException
     */
    private function validateDepartment(Department $department): void
    {
        $this->validator->validate($department);
    }
}
