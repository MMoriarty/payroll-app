<?php

declare(strict_types=1);

namespace Payroll\Department;

use Payroll\Department\Exception\DuplicatedDepartmentNameException;

interface DepartmentPersister
{
    /**
     * @throws DuplicatedDepartmentNameException
     */
    public function create(Department $department): void;
}
