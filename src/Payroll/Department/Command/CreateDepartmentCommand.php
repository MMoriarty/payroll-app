<?php

declare(strict_types=1);

namespace Payroll\Department\Command;

class CreateDepartmentCommand
{
    public function __construct(
        public readonly string $departmentName,
        public readonly string $salaryAddition,
    ) {}
}
