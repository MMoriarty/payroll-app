<?php

declare(strict_types=1);

namespace Payroll;

abstract class PayrollException extends \Exception
{
}
