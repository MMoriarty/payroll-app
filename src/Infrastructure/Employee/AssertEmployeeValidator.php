<?php

declare(strict_types=1);

namespace Infrastructure\Employee;

use Payroll\Employee\Employee;
use Payroll\Employee\EmployeeValidator;
use Payroll\ValidationException;
use Webmozart\Assert\Assert;
use Webmozart\Assert\InvalidArgumentException;

class AssertEmployeeValidator implements EmployeeValidator
{
    private const FIRST_NAME_EMPTY = 'First name cannot be empty.';
    private const FIRST_NAME_TOO_LONG = 'First name cannot be longer than 250 characters.';
    private const LAST_NAME_EMPTY = 'Last name cannot be empty.';
    private const LAST_NAME_TOO_LONG = 'Last name cannot be longer than 250 characters.';
    private const SALARY_CANNOT_BE_EQUAL_TO_ZERO = 'Salary cannot be equal to 0.';

    public function validate(Employee $employee): void
    {
        try {
            Assert::notEmpty($employee->firstName, static::FIRST_NAME_EMPTY);
            Assert::maxLength($employee->firstName, 250, static::FIRST_NAME_TOO_LONG);

            Assert::notEmpty($employee->lastName, static::LAST_NAME_EMPTY);
            Assert::maxLength($employee->lastName, 250, static::LAST_NAME_TOO_LONG);

            Assert::greaterThanEq($employee->salary, 1, static::SALARY_CANNOT_BE_EQUAL_TO_ZERO);
            Assert::notNull($employee->employmentDate, 'Expected date format is "Y-m-d".');
        } catch (InvalidArgumentException $e) {
            throw ValidationException::create($e->getMessage(), $e);
        }
    }
}
