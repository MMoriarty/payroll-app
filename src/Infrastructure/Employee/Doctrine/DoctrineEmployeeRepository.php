<?php

declare(strict_types=1);

namespace Infrastructure\Employee\Doctrine;

use Doctrine\ORM\EntityManagerInterface;
use Doctrine\ORM\EntityRepository;
use Doctrine\Persistence\ObjectRepository;
use Infrastructure\Employee\Doctrine\Employee as EmployeeEntity;
use Payroll\Employee\Filter\FilterBy;
use Payroll\Employee\EmployeeRepository;
use Payroll\Employee\Sorting\SortBy;
use Payroll\Employee\Sorting\SortOrder;
use Symfony\Component\Uid\Uuid;

class DoctrineEmployeeRepository implements EmployeeRepository
{
    protected EmployeeFactory $employeeFactory;
    protected ObjectRepository|EntityRepository $repository;

    public function __construct(EntityManagerInterface $entityManager, EmployeeFactory $employeeFactory)
    {
        $this->repository = $entityManager->getRepository(EmployeeEntity::class);
        $this->employeeFactory = $employeeFactory;
    }

    public function findMany(
        FilterBy $filter = null,
        SortBy $sortBy = null,
        SortOrder $sortOrder = SortOrder::ASC
    ): array {
        $qb = $this->repository->createQueryBuilder('e');
        $qb->join('e.department', 'd');

        if ($filter?->firstName !== null) {
            $qb->andWhere('e.firstName = :firstName')->setParameter('firstName', $filter->firstName);
        }

        if ($filter?->lastName !== null) {
            $qb->andWhere('e.lastName = :lastName')->setParameter('lastName', $filter->lastName);
        }

        if ($filter?->department?->id !== null) {
            $qb->andWhere('d.id = :departmentId')->setParameter(
                'departmentId',
                Uuid::fromString($filter->department->id)->toBinary(),
            );
        }

        if (null !== $sortBy && null !== $sortingField = $this->mapSortByToField($sortBy)) {
            $qb->orderBy($sortingField, $sortOrder->value);
        }

        $qb->addOrderBy('e.employmentDate', $sortOrder->value);

        return \array_map(
            fn(Employee $employee) => $this->employeeFactory->createEmployeeFromEntity($employee),
            $qb->getQuery()->getResult()
        );
    }

    private function mapSortByToField(SortBy $sortBy): ?string
    {
        return match ($sortBy) {
            SortBy::FIRST_NAME => 'e.firstName',
            SortBy::LAST_NAME => 'e.lastName',
            SortBy::DEPARTMENT => 'd.name',
            SortBy::BASE_SALARY => 'e.salary',
            SortBy::SALARY_ADDITION_TYPE => 'd.salaryAdditionType',
            default => null,
        };
    }
}
