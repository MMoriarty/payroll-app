<?php

declare(strict_types=1);

namespace Infrastructure\Employee\Doctrine;

use Doctrine\ORM\EntityManagerInterface;
use Payroll\Employee\Employee;
use Payroll\Employee\EmployeePersister;

class DoctrineEmployeePersister implements EmployeePersister
{
    protected EmployeeFactory $employeeFactory;
    protected EntityManagerInterface $entityManager;

    public function __construct(EmployeeFactory $employeeFactory, EntityManagerInterface $entityManager)
    {
        $this->employeeFactory = $employeeFactory;
        $this->entityManager = $entityManager;
    }

    public function create(Employee $employee): void
    {
        $this->entityManager->persist($this->employeeFactory->createEntityFromEmployee($employee));
        $this->entityManager->flush();
    }
}
