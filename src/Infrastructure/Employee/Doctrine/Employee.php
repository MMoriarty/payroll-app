<?php

declare(strict_types=1);

namespace Infrastructure\Employee\Doctrine;

use Doctrine\ORM\Mapping\Column;
use Doctrine\ORM\Mapping\Entity;
use Doctrine\ORM\Mapping\Id;
use Doctrine\ORM\Mapping\Index;
use Doctrine\ORM\Mapping\JoinColumn;
use Doctrine\ORM\Mapping\ManyToOne;
use Doctrine\ORM\Mapping\Table;
use Infrastructure\Department\Doctrine\Department;
use Symfony\Component\Uid\Uuid;

#[Entity]
#[Table(name: 'employees')]
#[Index(columns: ['first_name'], name: 'first_name_idx')]
#[Index(columns: ['last_name'], name: 'last_name_idx')]
#[Index(columns: ['salary'], name: 'salary_idx')]
#[Index(columns: ['employment_date'], name: 'employment_date_idx')]
class Employee
{
    #[Id]
    #[Column(name: 'id', type: 'uuid')]
    private Uuid $id;

    #[ManyToOne(targetEntity: Department::class, inversedBy: 'employees')]
    #[JoinColumn(name: 'department_id', referencedColumnName: 'id')]
    private Department $department;

    #[Column(name: 'first_name', type: 'string', length: 255)]
    private string $firstName;

    #[Column(name: 'last_name', type: 'string', length: 255)]
    private string $lastName;

    #[Column(name: 'salary', type: 'bigint')]
    private int $salary;

    #[Column(name: 'employment_date', type: 'date')]
    private \DateTime $employmentDate;

    public function __construct(
        Department $department,
        string $firstName,
        string $lastName,
        int $salary,
        \DateTime $employmentDate,
        Uuid $id = null,
    ) {
        $this->id = $id ?? Uuid::v4();
        $this->department = $department;
        $this->firstName = $firstName;
        $this->lastName = $lastName;
        $this->salary = $salary;
        $this->employmentDate = $employmentDate;
    }

    public function getId(): Uuid
    {
        return $this->id;
    }

    public function setId(Uuid $id): void
    {
        $this->id = $id;
    }

    public function getDepartment(): Department
    {
        return $this->department;
    }

    public function setDepartment(Department $department): void
    {
        $this->department = $department;
    }

    public function getFirstName(): string
    {
        return $this->firstName;
    }

    public function setFirstName(string $firstName): void
    {
        $this->firstName = $firstName;
    }

    public function getLastName(): string
    {
        return $this->lastName;
    }

    public function setLastName(string $lastName): void
    {
        $this->lastName = $lastName;
    }

    public function getSalary(): int
    {
        return $this->salary;
    }

    public function setSalary(int $salary): void
    {
        $this->salary = $salary;
    }

    public function getEmploymentDate(): \DateTime
    {
        return $this->employmentDate;
    }

    public function setEmploymentDate(\DateTime $employmentDate): void
    {
        $this->employmentDate = $employmentDate;
    }
}
