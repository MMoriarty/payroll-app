<?php

declare(strict_types=1);

namespace Infrastructure\Employee\Doctrine;

use Doctrine\ORM\EntityManagerInterface;
use Infrastructure\Department\Doctrine\Department;
use Infrastructure\Department\Doctrine\DepartmentFactory;
use Infrastructure\Employee\Doctrine\Employee as EmployeeEntity;
use Payroll\Employee\Employee;
use Symfony\Component\Uid\Uuid;

class EmployeeFactory
{
    protected DepartmentFactory $departmentFactory;
    protected EntityManagerInterface $entityManager;

    public function __construct(DepartmentFactory $departmentFactory, EntityManagerInterface $entityManager)
    {
        $this->departmentFactory = $departmentFactory;
        $this->entityManager = $entityManager;
    }

    public function createEmployeeFromEntity(EmployeeEntity $employee): Employee
    {
        return new Employee(
            $employee->getFirstName(),
            $employee->getLastName(),
            $employee->getSalary(),
            $this->departmentFactory->createDepartmentFromEntity($employee->getDepartment()),
            $employee->getEmploymentDate(),
            $employee->getId()->toRfc4122(),
        );
    }

    public function createEntityFromEmployee(Employee $employee): EmployeeEntity
    {
        if ($employee->department->id !== null) {
            $department = $this->entityManager->find(Department::class, $employee->department->id);
        }

        return new EmployeeEntity(
            ($department ?? $this->departmentFactory->createEntityFromDepartment($employee->department)),
            $employee->firstName,
            $employee->lastName,
            $employee->salary,
            $employee->employmentDate,
            ($employee->id !== null ? Uuid::fromString($employee->id) : null),
        );
    }
}
