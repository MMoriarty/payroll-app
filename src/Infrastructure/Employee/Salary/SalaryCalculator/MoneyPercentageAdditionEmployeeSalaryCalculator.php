<?php

declare(strict_types=1);

namespace Infrastructure\Employee\Salary\SalaryCalculator;

use Infrastructure\Employee\Salary\MoneyCurrencyFactory;
use Money\Money;
use Payroll\Employee\Employee;
use Payroll\Employee\Salary\EmployeeSalary;
use Payroll\Employee\Salary\Exception\SalaryCalculationModeNotSupportedException;
use Payroll\Employee\Salary\SalaryAddition\SalaryAdditionType;
use Payroll\Employee\Salary\SalaryCalculator\EmployeeSalaryCalculator;

class MoneyPercentageAdditionEmployeeSalaryCalculator implements EmployeeSalaryCalculator
{
    protected MoneyCurrencyFactory $currencyFactory;

    public function __construct(MoneyCurrencyFactory $currencyFactory)
    {
        $this->currencyFactory = $currencyFactory;
    }

    public function calculateForEmployee(Employee $employee): EmployeeSalary
    {
        if (!$this->supports($employee)) {
            throw SalaryCalculationModeNotSupportedException::specific($employee->department->salaryAddition, $this);
        }

        $salary = new Money($employee->salary, $this->currencyFactory->createCurrencyFromGlobal());
        $addition = $salary->multiply($employee->department->salaryAddition->value)->divide(100);
        $total = $salary->add($addition);

        return new EmployeeSalary($employee, (int)$addition->getAmount(), (int)$total->getAmount());
    }

    public function supports(Employee $employee): bool
    {
        return $employee->department->salaryAddition->type === SalaryAdditionType::PERCENTAGE;
    }
}
