<?php

declare(strict_types=1);

namespace Infrastructure\Employee\Salary\SalaryCalculator;

use Payroll\Employee\Employee;
use Payroll\Employee\Salary\EmployeeSalary;
use Payroll\Employee\Salary\Exception\SalaryCalculationModeNotSupportedException;
use Payroll\Employee\Salary\SalaryCalculator\EmployeeSalaryCalculator;

class EmployeeSalaryCalculationStrategy implements EmployeeSalaryCalculator
{
    /**
     * @var EmployeeSalaryCalculator[]
     */
    protected array $employeeSalaryCalculators;

    public function __construct(EmployeeSalaryCalculator ...$employeeSalaryCalculators)
    {
        $this->employeeSalaryCalculators = $employeeSalaryCalculators;
    }

    /**
     * @throws SalaryCalculationModeNotSupportedException
     */
    public function calculateForEmployee(Employee $employee): EmployeeSalary
    {
        foreach ($this->employeeSalaryCalculators as $employeeSalaryCalculator) {
            if ($employeeSalaryCalculator->supports($employee)) {
                return $employeeSalaryCalculator->calculateForEmployee($employee);
            }
        }

        throw SalaryCalculationModeNotSupportedException::any($employee->department->salaryAddition);
    }

    public function supports(Employee $employee): bool
    {
        foreach ($this->employeeSalaryCalculators as $employeeSalaryCalculator) {
            if ($employeeSalaryCalculator->supports($employee)) {
                return true;
            }
        }

        return false;
    }
}
