<?php

declare(strict_types=1);

namespace Infrastructure\Employee\Salary\SalaryCalculator;

use App\Clock\Clock;
use Infrastructure\Employee\Salary\MoneyCurrencyFactory;
use Money\Money;
use Payroll\Employee\Employee;
use Payroll\Employee\Salary\EmployeeSalary;
use Payroll\Employee\Salary\Exception\SalaryCalculationModeNotSupportedException;
use Payroll\Employee\Salary\SalaryAddition\SalaryAdditionType;
use Payroll\Employee\Salary\SalaryCalculator\EmployeeSalaryCalculator;

class MoneyFixedAdditionEmployeeSalaryCalculator implements EmployeeSalaryCalculator
{
    protected MoneyCurrencyFactory $currencyFactory;
    protected Clock $clock;

    public function __construct(MoneyCurrencyFactory $currencyFactory, Clock $clock)
    {
        $this->currencyFactory = $currencyFactory;
        $this->clock = $clock;
    }

    public function calculateForEmployee(Employee $employee): EmployeeSalary
    {
        if (!$this->supports($employee)) {
            throw SalaryCalculationModeNotSupportedException::specific($employee->department->salaryAddition, $this);
        }

        $salary = new Money($employee->salary, $this->currencyFactory->createCurrencyFromGlobal());
        $addition = new Money(
            $employee->department->salaryAddition->value,
            $this->currencyFactory->createCurrencyFromGlobal()
        );
        $totalAddition = $addition->multiply($this->getSalaryMultiplier($employee));
        $totalSalary = $salary->add($totalAddition);

        return new EmployeeSalary($employee, (int)$totalAddition->getAmount(), (int)$totalSalary->getAmount());
    }

    public function supports(Employee $employee): bool
    {
        return $employee->department->salaryAddition->type === SalaryAdditionType::FIXED;
    }

    private function getSalaryMultiplier(Employee $employee): int
    {
        $now = $this->clock->now();
        $interval = \date_diff($employee->employmentDate, $now);
        $years = $interval->y;

        if ($now->format('m') === $employee->employmentDate->format('m') && $interval->m === 11) {
            $years++;
        }

        return match (true) {
            $interval->invert === 1 => 0,
            $years > 10 => 10,
            default => $years,
        };
    }
}
