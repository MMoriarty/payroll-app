<?php

declare(strict_types=1);

namespace Infrastructure\Employee\Salary;

use Money\Currencies\ISOCurrencies;
use Money\Currency;
use Money\Exception\ParserException;
use Money\Parser\DecimalMoneyParser;
use Payroll\Employee\Salary\SalaryParser;
use Payroll\ValidationException;

class MoneySalaryParser implements SalaryParser
{
    protected DecimalMoneyParser $decimalMoneyParser;
    protected MoneyCurrencyFactory $currencyFactory;

    public function __construct(MoneyCurrencyFactory $currencyFactory, DecimalMoneyParser $decimalMoneyParser)
    {
        $this->currencyFactory = $currencyFactory;
        $this->decimalMoneyParser = $decimalMoneyParser;
    }

    /**
     * @throws ValidationException
     */
    public function parse(string $value): int
    {
        try {
            return (int)$this->decimalMoneyParser->parse($value, $this->currencyFactory->createCurrencyFromGlobal())->getAmount();
        } catch (ParserException $e) {
            throw ValidationException::create(\sprintf('Invalid salary value "%s".', $value), $e);
        }
    }
}
