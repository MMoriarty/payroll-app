<?php

declare(strict_types=1);

namespace Infrastructure\Employee\Salary;

use Money\Currency;

class MoneyCurrencyFactory
{
    protected string $currency;

    public function __construct(string $currency)
    {
        $this->currency = $currency;
    }

    public function createCurrencyFromGlobal(): Currency
    {
        return new Currency($this->currency);
    }
}
