<?php

declare(strict_types=1);

namespace Infrastructure\Employee\Salary;

use Money\Currencies\ISOCurrencies;
use Money\Currency;
use Money\Formatter\DecimalMoneyFormatter;
use Money\Money;
use Payroll\Employee\Salary\SalaryFormatter;

class MoneySalaryFormatter implements SalaryFormatter
{
    protected MoneyCurrencyFactory $currencyFactory;
    protected DecimalMoneyFormatter $moneyFormatter;

    public function __construct(MoneyCurrencyFactory $currencyFactory)
    {
        $this->moneyFormatter = new DecimalMoneyFormatter(new ISOCurrencies());
        $this->currencyFactory = $currencyFactory;
    }

    public function format(int $value): string
    {
        return $this->moneyFormatter->format(new Money($value, $this->currencyFactory->createCurrencyFromGlobal()));
    }
}
