<?php

declare(strict_types=1);

namespace Infrastructure\Employee\Salary\SalaryAddition\Extractor;

use Payroll\Employee\Salary\Exception\SalaryAdditionValueNotSupportedException;
use Payroll\Employee\Salary\SalaryAddition\Extractor\SalaryAdditionExtractor;
use Payroll\Employee\Salary\SalaryAddition\SalaryAddition;

class SalaryAdditionExtractionStrategy implements SalaryAdditionExtractor
{
    protected array $salaryAdditionExtractors;

    public function __construct(SalaryAdditionExtractor ...$salaryAdditionExtractors)
    {
        $this->salaryAdditionExtractors = $salaryAdditionExtractors;
    }

    public function extractFrom(string $value): SalaryAddition
    {
        foreach ($this->salaryAdditionExtractors as $salaryAdditionExtractor) {
            if ($salaryAdditionExtractor->supports($value)) {
                return $salaryAdditionExtractor->extractFrom($value);
            }
        }

        throw SalaryAdditionValueNotSupportedException::create($value);
    }

    public function supports(string $value): bool
    {
        foreach ($this->salaryAdditionExtractors as $salaryAdditionExtractor) {
            if ($salaryAdditionExtractor->supports($value)) {
                return true;
            }
        }

        return false;
    }
}
