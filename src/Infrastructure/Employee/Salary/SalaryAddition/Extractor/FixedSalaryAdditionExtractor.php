<?php

declare(strict_types=1);

namespace Infrastructure\Employee\Salary\SalaryAddition\Extractor;

use Payroll\Employee\Salary\Exception\SalaryAdditionValueNotSupportedException;
use Payroll\Employee\Salary\SalaryAddition\Extractor\SalaryAdditionExtractor;
use Payroll\Employee\Salary\SalaryAddition\SalaryAddition;
use Payroll\Employee\Salary\SalaryAddition\SalaryAdditionType;
use Payroll\Employee\Salary\SalaryParser;

class FixedSalaryAdditionExtractor implements SalaryAdditionExtractor
{
    protected SalaryParser $salaryParser;

    public function __construct(SalaryParser $salaryParser)
    {
        $this->salaryParser = $salaryParser;
    }

    public function extractFrom(string $value): SalaryAddition
    {
        if (!$this->supports($value)) {
            throw SalaryAdditionValueNotSupportedException::create($value);
        }

        return new SalaryAddition(SalaryAdditionType::FIXED, $this->salaryParser->parse($value));
    }

    public function supports(string $value): bool
    {
        return (bool)\preg_match('/^([1-9][0-9]*|0)(\.[0-9]{2})?$/', $value);
    }
}
