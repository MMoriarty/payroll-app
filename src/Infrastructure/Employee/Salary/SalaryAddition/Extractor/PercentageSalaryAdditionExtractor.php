<?php

declare(strict_types=1);

namespace Infrastructure\Employee\Salary\SalaryAddition\Extractor;

use Payroll\Employee\Salary\Exception\SalaryAdditionValueNotSupportedException;
use Payroll\Employee\Salary\Exception\SalaryAdditionValueOutOufScopeException;
use Payroll\Employee\Salary\SalaryAddition\Extractor\SalaryAdditionExtractor;
use Payroll\Employee\Salary\SalaryAddition\SalaryAddition;
use Payroll\Employee\Salary\SalaryAddition\SalaryAdditionType;

class PercentageSalaryAdditionExtractor implements SalaryAdditionExtractor
{
    /**
     * @throws SalaryAdditionValueNotSupportedException
     * @throws SalaryAdditionValueOutOufScopeException
     */
    public function extractFrom(string $value): SalaryAddition
    {
        $value = $this->getPercentageNumberFromValue($value);
        $this->assertPercentageIsInSupportedRange($value);

        return new SalaryAddition(SalaryAdditionType::PERCENTAGE, $value);
    }

    public function supports(string $value): bool
    {
        return (bool)\preg_match('/^(\d+%)$/', $value);
    }

    /**
     * @throws SalaryAdditionValueNotSupportedException
     */
    private function getPercentageNumberFromValue(string $value): int
    {
        \preg_match_all('/^(\d+)%$/', $value, $matches);
        $percentage = $matches[1][0] ?? null;

        if ($percentage === null) {
            throw SalaryAdditionValueNotSupportedException::create($value);
        }

        return (int)$percentage;
    }

    /**
     * @throws SalaryAdditionValueOutOufScopeException
     */
    private function assertPercentageIsInSupportedRange(int $value): void
    {
        if ($value < 1 || $value > 100) {
            throw SalaryAdditionValueOutOufScopeException::create(
                'Salary percentage value cannot be lower than 1% and higher than 100%.'
            );
        }
    }
}
