<?php

declare(strict_types=1);

namespace Infrastructure\Department\Doctrine;

use Doctrine\ORM\EntityManagerInterface;
use Doctrine\ORM\EntityRepository;
use Doctrine\Persistence\ObjectRepository;
use Infrastructure\Department\Doctrine\Department as DepartmentEntity;
use Payroll\Department\Department;
use Payroll\Department\DepartmentRepository;

class DoctrineDepartmentRepository implements DepartmentRepository
{
    protected DepartmentFactory $departmentFactory;
    protected ObjectRepository|EntityRepository $repository;

    public function __construct(EntityManagerInterface $entityManager, DepartmentFactory $departmentFactory)
    {
        $this->repository = $entityManager->getRepository(DepartmentEntity::class);
        $this->departmentFactory = $departmentFactory;
    }

    public function find(string $id): ?Department
    {
        $entity = $this->repository->findOneBy(['id' => $id]);
        if ($entity === null) {
            return null;
        }

        return $this->departmentFactory->createDepartmentFromEntity($entity);
    }

    public function findByName(string $name): ?Department
    {
        $entity = $this->repository->findOneBy(['name' => $name]);
        if ($entity === null) {
            return null;
        }

        return $this->departmentFactory->createDepartmentFromEntity($entity);
    }
}
