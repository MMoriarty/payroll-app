<?php

declare(strict_types=1);

namespace Infrastructure\Department\Doctrine;

use Infrastructure\Department\Doctrine\Department as DepartmentEntity;
use Payroll\Department\Department;
use Payroll\Employee\Salary\SalaryAddition\SalaryAddition;
use Symfony\Component\Uid\Uuid;

class DepartmentFactory
{
    public function createDepartmentFromEntity(DepartmentEntity $entity): Department
    {
        return new Department(
            $entity->getName(),
            new SalaryAddition($entity->getSalaryAdditionType(), $entity->getSalaryAdditionValue()),
            $entity->getId()->toRfc4122(),
        );
    }

    public function createEntityFromDepartment(Department $department): DepartmentEntity
    {
        return new DepartmentEntity(
            $department->name,
            $department->salaryAddition->type,
            $department->salaryAddition->value,
            ($department->id !== null ? Uuid::fromRfc4122($department->id) : null),
        );
    }
}
