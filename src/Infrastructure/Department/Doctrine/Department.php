<?php

declare(strict_types=1);

namespace Infrastructure\Department\Doctrine;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping\Column;
use Doctrine\ORM\Mapping\Entity;
use Doctrine\ORM\Mapping\Id;
use Doctrine\ORM\Mapping\Index;
use Doctrine\ORM\Mapping\OneToMany;
use Doctrine\ORM\Mapping\Table;
use Infrastructure\Employee\Doctrine\Employee;
use Payroll\Employee\Salary\SalaryAddition\SalaryAdditionType;
use Symfony\Component\Uid\Uuid;

#[Entity]
#[Table(name: 'departments')]
#[Index(columns: ['name'], name: 'name_idx')]
#[Index(columns: ['salary_addition_type'], name: 'salary_addition_type_idx')]
#[Index(columns: ['salary_addition_value'], name: 'salary_addition_value_idx')]
class Department
{
    #[Id]
    #[Column(name: 'id', type: 'uuid')]
    private Uuid $id;

    #[Column(name: 'name', type: 'string', length: 255, unique: true)]
    private string $name;

    #[Column(name: 'salary_addition_type', type: 'smallint')]
    private int $salaryAdditionType;

    #[Column(name: 'salary_addition_value', type: 'bigint')]
    private int $salaryAdditionValue;

    #[OneToMany(mappedBy: 'department', targetEntity: Employee::class)]
    private Collection $employees;

    public function __construct(
        string $name = '',
        SalaryAdditionType $salaryAdditionType = SalaryAdditionType::FIXED,
        int $salaryAdditionValue = 0,
        Uuid $id = null
    ) {
        $this->id = $id ?? Uuid::v4();
        $this->name = $name;
        $this->salaryAdditionType = $salaryAdditionType->value;
        $this->salaryAdditionValue = $salaryAdditionValue;
        $this->employees = new ArrayCollection();
    }

    public function getId(): Uuid
    {
        return $this->id;
    }

    public function setId(Uuid $id): void
    {
        $this->id = $id;
    }

    public function getName(): string
    {
        return $this->name;
    }

    public function setName(string $name): void
    {
        $this->name = $name;
    }

    public function getSalaryAdditionType(): SalaryAdditionType
    {
        return SalaryAdditionType::from($this->salaryAdditionType);
    }

    public function setSalaryAdditionType(SalaryAdditionType $salaryAdditionType): void
    {
        $this->salaryAdditionType = $salaryAdditionType->value;
    }

    public function getSalaryAdditionValue(): int
    {
        return $this->salaryAdditionValue;
    }

    public function setSalaryAdditionValue(int $salaryAdditionValue): void
    {
        $this->salaryAdditionValue = $salaryAdditionValue;
    }

    public function getEmployees(): Collection
    {
        return $this->employees;
    }
}
