<?php

declare(strict_types=1);

namespace Infrastructure\Department\Doctrine;

use Doctrine\DBAL\Exception\UniqueConstraintViolationException;
use Doctrine\ORM\EntityManagerInterface;
use Payroll\Department\Department;
use Payroll\Department\Exception\DuplicatedDepartmentNameException;
use Payroll\Department\DepartmentPersister;

class DoctrineDepartmentPersister implements DepartmentPersister
{
    protected EntityManagerInterface $entityManager;
    protected DepartmentFactory $factory;

    public function __construct(EntityManagerInterface $entityManager, DepartmentFactory $factory)
    {
        $this->entityManager = $entityManager;
        $this->factory = $factory;
    }

    public function create(Department $department): void
    {
        try {
            $this->entityManager->persist($this->factory->createEntityFromDepartment($department));
            $this->entityManager->flush();
        } catch (UniqueConstraintViolationException) {
            throw DuplicatedDepartmentNameException::create($department->name);
        }
    }
}
