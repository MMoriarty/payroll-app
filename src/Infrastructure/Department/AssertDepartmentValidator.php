<?php

declare(strict_types=1);

namespace Infrastructure\Department;

use Payroll\Department\Department;
use Payroll\Department\DepartmentValidator;
use Payroll\ValidationException;
use Webmozart\Assert\Assert;
use Webmozart\Assert\InvalidArgumentException;

class AssertDepartmentValidator implements DepartmentValidator
{
    private const DEPARTMENT_NAME_EMPTY = 'Department name cannot be empty.';
    private const DEPARTMENT_NAME_TOO_LONG = 'Department name cannot be longer than 250 characters.';

    public function validate(Department $department): void
    {
        try {
            Assert::notEmpty($department->name, static::DEPARTMENT_NAME_EMPTY);
            Assert::maxLength($department->name, 250, static::DEPARTMENT_NAME_TOO_LONG);
        } catch (InvalidArgumentException $e) {
            throw ValidationException::create($e->getMessage(), $e);
        }
    }
}
