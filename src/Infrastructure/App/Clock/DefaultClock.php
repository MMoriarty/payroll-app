<?php

declare(strict_types=1);

namespace Infrastructure\App\Clock;

use App\Clock\Clock;

class DefaultClock implements Clock
{
    public function now(): \DateTime
    {
        return new \DateTime();
    }

    public function createFromFormat(string $format, string $value): ?\DateTime
    {
        $result = \DateTime::createFromFormat($format, $value);
        if (!$result) {
            return null;
        }

        return $result;
    }
}
