<?php

declare(strict_types=1);

namespace Infrastructure\App\Bus;

use App\Bus\QueryBus;
use League\Tactician\CommandBus as DecoratedCommandBus;

class TacticianQueryBus implements QueryBus
{
    protected DecoratedCommandBus $commandBus;

    public function __construct(DecoratedCommandBus $commandBus)
    {
        $this->commandBus = $commandBus;
    }

    public function handle(object $query): mixed
    {
        return $this->commandBus->handle($query);
    }
}
