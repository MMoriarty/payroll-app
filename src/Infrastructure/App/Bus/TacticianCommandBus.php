<?php

declare(strict_types=1);

namespace Infrastructure\App\Bus;

use App\Bus\CommandBus;
use League\Tactician\CommandBus as DecoratedCommandBus;

class TacticianCommandBus implements CommandBus
{
    protected DecoratedCommandBus $commandBus;

    public function __construct(DecoratedCommandBus $commandBus)
    {
        $this->commandBus = $commandBus;
    }

    public function handle(object $command): void
    {
        $this->commandBus->handle($command);
    }
}
