<?php

declare(strict_types=1);

namespace App;

use App\Bus\CommandBus;
use App\Bus\QueryBus;
use Symfony\Component\Console\Command\Command as SymfonyCommand;

abstract class Command extends SymfonyCommand
{
    protected readonly CommandBus $commandBus;
    protected readonly QueryBus $queryBus;

    public function __construct(CommandBus $commandBus, QueryBus $queryBus)
    {
        parent::__construct();

        $this->commandBus = $commandBus;
        $this->queryBus = $queryBus;
    }
}
