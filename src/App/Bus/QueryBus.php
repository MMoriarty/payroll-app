<?php

declare(strict_types=1);

namespace App\Bus;

interface QueryBus
{
    public function handle(object $query): mixed;
}
