<?php

declare(strict_types=1);

namespace App\Command;

use App\Command;
use Payroll\Employee\Command\CreateEmployeeCommand;
use Payroll\PayrollException;
use Symfony\Component\Console\Attribute\AsCommand;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Style\SymfonyStyle;

#[AsCommand(name: 'payroll:employee:add', description: 'Adds new employee.')]
class PayrollEmployeeAddCommand extends Command
{
    protected function configure(): void
    {
        $this->addArgument(
            'first-name',
            InputArgument::REQUIRED,
            'First name of the new employee.'
        );

        $this->addArgument(
            'last-name',
            InputArgument::REQUIRED,
            'Last name of the new employee.'
        );

        $this->addArgument(
            'department-name',
            InputArgument::REQUIRED,
            'Department to which the employee will be assigned.',
        );

        $this->addArgument(
            'salary',
            InputArgument::REQUIRED,
            'Employee base salary.',
        );

        $this->addOption(
            name: 'employment-date',
            mode: InputOption::VALUE_REQUIRED,
            description: 'Set custom date of employment. By default current date will be used.'
        );
    }

    protected function execute(InputInterface $input, OutputInterface $output): int
    {
        $io = new SymfonyStyle($input, $output);

        $firstName = $input->getArgument('first-name');
        $lastName = $input->getArgument('last-name');
        $salary = $input->getArgument('salary');
        $departmentName = $input->getArgument('department-name');
        $employmentDate = $input->getOption('employment-date');

        try {
            $this->commandBus->handle(new CreateEmployeeCommand(
                $firstName,
                $lastName,
                $departmentName,
                $salary,
                $employmentDate
            ));

            $io->success('New employee has been added.');
        } catch (PayrollException $e) {
            $io->error($e->getMessage());
            return Command::FAILURE;
        }

        return Command::SUCCESS;
    }
}
