<?php

declare(strict_types=1);

namespace App\Command;

use App\Command;
use Payroll\Department\Command\CreateDepartmentCommand;
use Payroll\PayrollException;
use Symfony\Component\Console\Attribute\AsCommand;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Style\SymfonyStyle;

#[AsCommand(name: 'payroll:department:add', description: 'Creates new department.')]
class PayrollDepartmentAddCommand extends Command
{
    protected function configure(): void
    {
        $this->addArgument(
            'department-name',
            InputArgument::REQUIRED,
            'Name for new department.'
        );

        $this->addArgument(
            'department-salary-addition',
            InputArgument::REQUIRED,
            <<<'EOF'
Addition to salary for each employee in department.
Can be defined as fixed value (an integer/float value) or percentage (for example: 10%).
EOF
        );
    }

    protected function execute(InputInterface $input, OutputInterface $output): int
    {
        $io = new SymfonyStyle($input, $output);

        $departmentName = (string)$input->getArgument('department-name');
        $salaryAddition = (string)$input->getArgument('department-salary-addition');

        try {
            $this->commandBus->handle(new CreateDepartmentCommand($departmentName, $salaryAddition));
            $io->success('Department has been created.');
        } catch (PayrollException $e) {
            $io->error($e->getMessage());
            return Command::FAILURE;
        }

        return Command::SUCCESS;
    }
}
