<?php

declare(strict_types=1);

namespace App\Command;

use App\Command;
use Payroll\Employee\Query\GetEmployeesSalaryReportQuery;
use Payroll\Employee\Salary\SalaryReport\SalaryReportCollection;
use Payroll\Employee\Sorting\SortBy;
use Payroll\Employee\Sorting\SortOrder;
use Payroll\PayrollException;
use Symfony\Component\Console\Attribute\AsCommand;
use Symfony\Component\Console\Helper\Table;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Style\SymfonyStyle;

#[AsCommand(name: 'payroll:salary:generate-report', description: 'Generates report with employees salaries.')]
class PayrollSalaryGenerateReportCommand extends Command
{
    protected function configure(): void
    {
        $this->addOption(
            name: 'filter-firstname',
            mode: InputOption::VALUE_REQUIRED,
            description: 'Show only report for employees with provided first name.',
        );

        $this->addOption(
            name: 'filter-lastname',
            mode: InputOption::VALUE_REQUIRED,
            description: 'Show only report for employees with provided last name.',
        );

        $this->addOption(
            name: 'filter-department-name',
            mode: InputOption::VALUE_REQUIRED,
            description: 'Show only report for employees in provided department.',
        );

        $this->addOption(
            name: 'sort-by',
            mode: InputOption::VALUE_REQUIRED,
            description: \sprintf('Sort list by column (available options: %s).', \join(', ', SortBy::options())),
        );

        $this->addOption(
            name: 'sort-desc',
            mode: InputOption::VALUE_NONE,
            description: 'Sort entries descending.'
        );
    }

    protected function execute(InputInterface $input, OutputInterface $output): int
    {
        $io = new SymfonyStyle($input, $output);

        $filterFirstname = $input->getOption('filter-firstname');
        $filterLastname = $input->getOption('filter-lastname');
        $filterDepartmentName = $input->getOption('filter-department-name');
        $sortBy = $input->getOption('sort-by');
        $sortOrder = $input->getOption('sort-desc')
            ? SortOrder::DESC->value
            : SortOrder::ASC->value;

        try {
            /** @var SalaryReportCollection $result */
            $result = $this->queryBus->handle(
                new GetEmployeesSalaryReportQuery(
                    $filterFirstname,
                    $filterLastname,
                    $filterDepartmentName,
                    $sortBy,
                    $sortOrder,
                )
            );
        } catch (PayrollException $e) {
            $io->error($e->getMessage());
            return Command::FAILURE;
        }

        if (\count($result) === 0) {
            $io->info('No employees found in database.');

            return Command::SUCCESS;
        }

        $table = new Table($output);
        $table->setStyle('box');
        $table->setHeaders([
            'First name',
            'Last name',
            'Department',
            'Base salary',
            'Salary addition',
            'Addition type',
            'Total salary',
        ]);

        foreach ($result as $item) {
            $table->addRow([
                $item->employee->firstName,
                $item->employee->lastName,
                $item->employee->department->name,
                $item->formattedBaseSalary,
                $item->formattedTotalSalaryAddition,
                $item->employee->department->salaryAddition->type->name(),
                $item->formattedFinalSalary,
            ]);
        }

        $table->render();

        return Command::SUCCESS;
    }
}
