# Payroll App
Simple payroll app to manage employees salaries.

## How to run?
To build all necessary images for application run:
```bash
make
```
After finalization of build process initialize project with:
```bash
make start
```
Application is ready to use!
## How to use?
If application has been initialized use command below to access commandline:
```bash
make cmd
```
All operations below can be executed inside CLI initialized by command above.
### Creating new department
```bash
app payroll:department:add <department-name> <department-salary-addition>
```
 * `<department-name>` - unique name for department.
 * `<department-salary-addition>` - salary addition for each employee in department.

Salary addition can be defined as fixed amount (for example: `1000.00` or just `1000`)
or percentage amount (for example: `25%`).
#### Examples
Department HR with 25% salary addition:
```bash
app payroll:department:add HR 25%
```
Department QA with 250.00 fixed salary addition:
```bash
app payroll:department:add QA 250
```
### Adding new employee
```bash
app payroll:employee:add <first-name> <last-name> <department-name> <salary> [--employment-date]
```
 * `<first-name>` - employee first name 
 * `<last-name>` - employee last name
 * `<department-name>` - name of department to which employee will be assigned
 * `<salary>` - employee salary (example values: `1500.00` ; `1500`)
 * `--employment-date` - optional; set custom employment date (format: `Y-m-d`; example: `2012-12-21`) by default current time will be used
#### Examples
Employee "John Doe" assigned to "HR" department with salary "2000.00":
```bash
app payroll:employee:add John Doe HR 2000
```
Employee "Sherlock Holmes" assigned to "QA" department with salary "1300.00":
```bash
app payroll:employee:add Sherlock Holmes QA 1300.00
```
Employee "Jane Doe" assigned to "QA" department with salary "753.50" employed on 2012-07-15:
```bash
app payroll:employee:add Jane Doe QA 753.50 --employment-date="2012-07-15"
```
### Generating report
```
app payroll:salary:generate-report [--filter-firstname] [--filter-lastname] [--filter-department-name] [--sort-by] [--sort-desc]
```
 * `--filter-firstname` - optional; show report only for employees with provided first name.
 * `--filter-lastname` - optional; show report only for employees with provided last name.
 * `--filter-department-name` - optional; show report only for employees in provided department.
 * `--sort-by` - optional; sort report results (supported fields: `First name`, `Last name`, `Department`, `Base salary`, `Salary addition`, `Addition type`, `Total salary`)
 * `--sort-desc` - optional; reverse sorting order (sort descending instead of ascending).
#### Examples
 Generate report for all employees.
```bash
app payroll:salary:generate-report
```
 Generate report for employees with first name "Sherlock".
```bash
app payroll:salary:generate-report --filter-firstname="Sherlock"
```
 Generate report for employees with last name "Doe".
```bash
app payroll:salary:generate-report --filter-lastname="Doe"
```
 Generate report for employees in "QA" department.
```bash
app payroll:salary:generate-report --filter-department-name="QA"
```
Generate report for employees with last name "Doe" and in "HR" department.
```bash
app payroll:salary:generate-report --filter-lastname="Doe" --filter-department-name="HR"
```
Generate report with entries sorted by base salary ascending.
```bash
app payroll:salary:generate-report --sort-by="Base salary"
```
Generate report with entries sorted by total salary descending.
```bash
app payroll:salary:generate-report --sort-by="Total salary" --sort-desc
```
## How to test?
If project has been already built with `make` then use command below to run tests:
```bash
make run-tests
```
