<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

final class Version20211216162645 extends AbstractMigration
{
    public function getDescription(): string
    {
        return '';
    }

    public function up(Schema $schema): void
    {
        $this->addSql('CREATE TABLE departments (id BINARY(16) NOT NULL COMMENT \'(DC2Type:uuid)\', name VARCHAR(255) NOT NULL, salary_addition_type SMALLINT NOT NULL, salary_addition_value BIGINT NOT NULL, UNIQUE INDEX UNIQ_16AEB8D45E237E06 (name), INDEX name_idx (name), INDEX salary_addition_type_idx (salary_addition_type), INDEX salary_addition_value_idx (salary_addition_value), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE employees (id BINARY(16) NOT NULL COMMENT \'(DC2Type:uuid)\', department_id BINARY(16) DEFAULT NULL COMMENT \'(DC2Type:uuid)\', first_name VARCHAR(255) NOT NULL, last_name VARCHAR(255) NOT NULL, salary BIGINT NOT NULL, employment_date DATE NOT NULL, INDEX IDX_BA82C300AE80F5DF (department_id), INDEX first_name_idx (first_name), INDEX last_name_idx (last_name), INDEX salary_idx (salary), INDEX employment_date_idx (employment_date), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('ALTER TABLE employees ADD CONSTRAINT FK_BA82C300AE80F5DF FOREIGN KEY (department_id) REFERENCES departments (id)');
    }

    public function down(Schema $schema): void
    {
        $this->addSql('ALTER TABLE employees DROP FOREIGN KEY FK_BA82C300AE80F5DF');
        $this->addSql('DROP TABLE departments');
        $this->addSql('DROP TABLE employees');
    }
}
