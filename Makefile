.PHONY: build
build: composer-install
	docker-compose -f docker-compose.yaml build --no-cache

.PHONY: cmd
cmd:
	docker-compose -f docker-compose.yaml run --rm --user $$(id -u):$$(id -g) app /bin/sh

.PHONY: start
start: stop
	docker-compose -f docker-compose.yaml -f docker-compose.prod.yaml up --detach --no-build --remove-orphans database
	docker-compose -f docker-compose.yaml -f docker-compose.prod.yaml run --rm wait
	docker-compose -f docker-compose.yaml -f docker-compose.prod.yaml run --rm --user $$(id -u):$$(id -g) app app cache:clear
	docker-compose -f docker-compose.yaml -f docker-compose.prod.yaml run --rm --user $$(id -u):$$(id -g) app app doctrine:migrations:migrate --no-interaction

.PHONY: stop
stop:
	docker-compose -f docker-compose.yaml -f docker-compose.prod.yaml down --timeout 3

.PHONY: run-tests
run-tests: clean-tests
	docker-compose -f docker-compose.yaml -f docker-compose.test.yaml up --detach --no-build --remove-orphans database
	docker-compose -f docker-compose.yaml -f docker-compose.test.yaml run --rm wait
	docker-compose -f docker-compose.yaml -f docker-compose.test.yaml run --rm --user $$(id -u):$$(id -g) app app cache:clear
	docker-compose -f docker-compose.yaml -f docker-compose.test.yaml run --rm --user $$(id -u):$$(id -g) app app doctrine:migrations:migrate --no-interaction
	docker-compose -f docker-compose.yaml -f docker-compose.test.yaml run --rm --user $$(id -u):$$(id -g) app codecept run

.PHONY: clean-tests
clean-tests:
	docker-compose -f docker-compose.yaml -f docker-compose.test.yaml down --timeout 3

.PHONY: dev
dev: clean-dev
	docker-compose -f docker-compose.yaml -f docker-compose.dev.yaml up --detach --no-build --remove-orphans database
	docker-compose -f docker-compose.yaml -f docker-compose.dev.yaml run --rm wait
	docker-compose -f docker-compose.yaml -f docker-compose.dev.yaml run --rm --user $$(id -u):$$(id -g) app app cache:clear
	docker-compose -f docker-compose.yaml -f docker-compose.dev.yaml run --rm --user $$(id -u):$$(id -g) app app doctrine:schema:update --force
	docker-compose -f docker-compose.yaml -f docker-compose.dev.yaml run --rm --user $$(id -u):$$(id -g) app /bin/sh

.PHONY: clean-dev
clean-dev:
	docker-compose -f docker-compose.yaml -f docker-compose.dev.yaml down --timeout 3

.PHONY: composer-install
composer-install:
	docker run --user $$(id -u):$$(id -g) --rm -it -v $$(pwd):/app composer:2.1 install --ignore-platform-reqs
