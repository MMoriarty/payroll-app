<?php

declare(strict_types=1);

namespace App\Tests\functional\Payroll\Employee\QueryHandler;

use App\Tests\FunctionalTester;
use Codeception\Example;
use Infrastructure\Employee\Doctrine\Employee;
use Payroll\Employee\Query\GetEmployeesSalaryReportQuery;
use Payroll\Employee\Salary\SalaryAddition\SalaryAdditionType;
use Payroll\Employee\Salary\SalaryReport\SalaryReportCollection;
use Payroll\Employee\Sorting\SortOrder;

class GetEmployeesSalaryReportQueryHandlerSortingCest
{
    private Employee $employeeAliceWonderlandQA;
    private Employee $employeeGerardEdlingQA;
    private Employee $employeeCharlesXavierHR;

    public function _before(FunctionalTester $I)
    {
        $departmentQA = $I->haveDepartment('QA', SalaryAdditionType::FIXED, 250_00);
        $departmentHR = $I->haveDepartment('HR', SalaryAdditionType::PERCENTAGE, 10);

        $this->employeeAliceWonderlandQA = $I->haveEmployee(
            department: $departmentQA,
            firstName: 'Alice',
            lastName: 'Wonderland',
            salary: 1000_00,
            employmentDate: new \DateTime('-1 day')
        );

        $this->employeeGerardEdlingQA = $I->haveEmployee(
            department: $departmentQA,
            firstName: 'Gerard',
            lastName: 'Edling',
            salary: 3000_00,
            employmentDate: new \DateTime('-3 years')
        );

        $this->employeeCharlesXavierHR = $I->haveEmployee(
            department: $departmentHR,
            firstName: 'Charles',
            lastName: 'Xavier',
            salary: 2800_00,
            employmentDate: new \DateTime('-3 day')
        );
    }

    /**
     * @dataProvider getSortingCases
     * @test
     */
    public function it_should_sort_report_items_based_on_provided_sorting_parameters_with_ascending_order(
        FunctionalTester $I,
        Example $example
    ): void {
        // GIVEN
        $sortingFiled = $example['sorting_field'];
        $sortingOrder = SortOrder::ASC->value;
        $itemsOrder = $example['expected_items_order']();
        $expectedFirstItemId = $itemsOrder[0]->getId()->toRfc4122();
        $expectedSecondItemId = $itemsOrder[1]->getId()->toRfc4122();
        $expectedThirdItemId = $itemsOrder[2]->getId()->toRfc4122();

        // WHEN
        /** @var SalaryReportCollection $result */
        $result = $I->runQuery(
            new GetEmployeesSalaryReportQuery(sortingField: $sortingFiled, sortingOrder: $sortingOrder)
        );

        // THEN
        $results = \iterator_to_array($result);

        $thirdItem = \array_pop($results);
        $I->assertEquals($expectedThirdItemId, $thirdItem->employee->id);

        $secondItem = \array_pop($results);
        $I->assertEquals($expectedSecondItemId, $secondItem->employee->id);

        $firstItem = \array_pop($results);
        $I->assertEquals($expectedFirstItemId, $firstItem->employee->id);
    }

    /**
     * @dataProvider getSortingCases
     * @test
     */
    public function it_should_sort_report_items_based_on_provided_sorting_parameters_with_descending_order(
        FunctionalTester $I,
        Example $example
    ): void {
        // GIVEN
        $sortingFiled = $example['sorting_field'];
        $sortingOrder = SortOrder::DESC->value;
        $itemsOrder = $example['expected_items_order']();
        $expectedFirstItemId = $itemsOrder[0]->getId()->toRfc4122();
        $expectedSecondItemId = $itemsOrder[1]->getId()->toRfc4122();
        $expectedThirdItemId = $itemsOrder[2]->getId()->toRfc4122();

        // WHEN
        /** @var SalaryReportCollection $result */
        $result = $I->runQuery(
            new GetEmployeesSalaryReportQuery(sortingField: $sortingFiled, sortingOrder: $sortingOrder)
        );

        // THEN
        $results = \iterator_to_array($result);

        $firstItem = \array_pop($results);
        $I->assertEquals($expectedFirstItemId, $firstItem->employee->id);

        $secondItem = \array_pop($results);
        $I->assertEquals($expectedSecondItemId, $secondItem->employee->id);

        $thirdItem = \array_pop($results);
        $I->assertEquals($expectedThirdItemId, $thirdItem->employee->id);
    }

    public function getSortingCases(): array
    {
        return [
            [
                'sorting_field'        => 'First name',
                'expected_items_order' => fn() => [
                    $this->employeeAliceWonderlandQA,
                    $this->employeeCharlesXavierHR,
                    $this->employeeGerardEdlingQA,
                ],
            ],
            [
                'sorting_field'        => 'Last name',
                'expected_items_order' => fn() => [
                    $this->employeeGerardEdlingQA,
                    $this->employeeAliceWonderlandQA,
                    $this->employeeCharlesXavierHR,
                ],
            ],
            [
                'sorting_field'        => 'Department',
                'expected_items_order' => fn() => [
                    $this->employeeCharlesXavierHR,
                    $this->employeeGerardEdlingQA,
                    $this->employeeAliceWonderlandQA,
                ],
            ],
            [
                'sorting_field'        => 'Base salary',
                'expected_items_order' => fn() => [
                    $this->employeeAliceWonderlandQA,
                    $this->employeeCharlesXavierHR,
                    $this->employeeGerardEdlingQA,
                ],
            ],
            [
                'sorting_field'        => 'Addition type',
                'expected_items_order' => fn() => [
                    $this->employeeGerardEdlingQA,
                    $this->employeeAliceWonderlandQA,
                    $this->employeeCharlesXavierHR,
                ],
            ],
            [
                'sorting_field'        => 'Salary addition',
                'expected_items_order' => fn() => [
                    $this->employeeAliceWonderlandQA,
                    $this->employeeCharlesXavierHR,
                    $this->employeeGerardEdlingQA,
                ],
            ],
            [
                'sorting_field'        => 'Total salary',
                'expected_items_order' => fn() => [
                    $this->employeeAliceWonderlandQA,
                    $this->employeeCharlesXavierHR,
                    $this->employeeGerardEdlingQA,
                ],
            ],
        ];
    }
}
