<?php

declare(strict_types=1);

namespace App\Tests\functional\Payroll\Employee\QueryHandler;

use App\Tests\FunctionalTester;
use Payroll\Employee\Query\GetEmployeesSalaryReportQuery;
use Payroll\Employee\Salary\SalaryAddition\SalaryAdditionType;
use Payroll\Employee\Salary\SalaryReport\SalaryReportCollection;

class GetEmployeesSalaryReportQueryHandlerCest
{
    /**
     * @test
     */
    public function it_should_generate_report_with_employees_salaries(FunctionalTester $I): void
    {
        // GIVEN
        $departmentQA = $I->haveDepartment('QA', SalaryAdditionType::FIXED, 250_00);
        $departmentHR = $I->haveDepartment('HR', SalaryAdditionType::PERCENTAGE, 10);
        $newestEmployee = $I->haveEmployee(
            department: $departmentQA,
            firstName: 'John',
            lastName: 'Doe',
            employmentDate: new \DateTime('-1 day')
        );
        $secondPlaceSeniorityEmployee = $I->haveEmployee(
            department: $departmentHR,
            firstName: 'Sherlock',
            lastName: 'Holmes',
            employmentDate: new \DateTime('-2 day')
        );
        $oldestEmployee = $I->haveEmployee(
            department: $departmentHR,
            firstName: 'Alice',
            lastName: 'Wonderland',
            employmentDate: new \DateTime('-3 day')
        );

        // WHEN
        /** @var SalaryReportCollection $result */
        $result = $I->runQuery(new GetEmployeesSalaryReportQuery());

        // THEN
        $results = \iterator_to_array($result);
        $I->assertCount(3, $results);

        $thirdEntryOnList = \array_pop($results);
        $I->assertEquals($newestEmployee->getId()->toRfc4122(), $thirdEntryOnList->employee->id);

        $secondEntryOnList = \array_pop($results);
        $I->assertEquals($secondPlaceSeniorityEmployee->getId()->toRfc4122(), $secondEntryOnList->employee->id);

        $firstEntryOnList = \array_pop($results);
        $I->assertEquals($oldestEmployee->getId()->toRfc4122(), $firstEntryOnList->employee->id);
    }
}
