<?php

declare(strict_types=1);

namespace App\Tests\functional\Payroll\Employee\QueryHandler;

use App\Tests\FunctionalTester;
use Infrastructure\Department\Doctrine\Department;
use Infrastructure\Employee\Doctrine\Employee;
use Payroll\Employee\Query\GetEmployeesSalaryReportQuery;
use Payroll\Employee\Salary\SalaryAddition\SalaryAdditionType;
use Payroll\Employee\Salary\SalaryReport\SalaryReportCollection;

class GetEmployeesSalaryReportQueryHandlerFilterCest
{
    private Employee $employeeJohnDoeQA;
    private Employee $employeeJaneDoeHR;
    private Employee $employeeJohnDoeHR;
    private Employee $employeeAliceWonderlandQA;

    public function _before(FunctionalTester $I)
    {
        $departmentQA = $I->haveDepartment('QA', SalaryAdditionType::FIXED, 250_00);
        $departmentHR = $I->haveDepartment('HR', SalaryAdditionType::PERCENTAGE, 10);
        $this->employeeJaneDoeHR = $I->haveEmployee(
            department: $departmentHR,
            firstName: 'Jane',
            lastName: 'Doe',
            employmentDate: new \DateTime('-1 day')
        );
        $this->employeeJohnDoeHR = $I->haveEmployee(
            department: $departmentHR,
            firstName: 'John',
            lastName: 'Doe',
            employmentDate: new \DateTime('-2 day')
        );
        $this->employeeJohnDoeQA = $I->haveEmployee(
            department: $departmentQA,
            firstName: 'John',
            lastName: 'Doe',
            employmentDate: new \DateTime('-3 day')
        );
        $this->employeeAliceWonderlandQA = $I->haveEmployee(
            department: $departmentQA,
            firstName: 'Alice',
            lastName: 'Wonderland',
            employmentDate: new \DateTime('-4 day')
        );
    }

    /**
     * @test
     */
    public function it_should_generate_report_with_employees_having_provided_first_name(FunctionalTester $I): void
    {
        // GIVEN
        $filteredFirstname = 'John';

        // WHEN
        /** @var SalaryReportCollection $result */
        $result = $I->runQuery(new GetEmployeesSalaryReportQuery(filterFirstname: $filteredFirstname));

        // THEN
        $results = \iterator_to_array($result);
        $I->assertCount(2, $results);

        $resultEmployeeJohnFirst = \array_pop($results);
        $I->assertEquals($this->employeeJohnDoeHR->getId()->toRfc4122(), $resultEmployeeJohnFirst->employee->id);

        $resultEmployeeJohnSecond = \array_pop($results);
        $I->assertEquals($this->employeeJohnDoeQA->getId()->toRfc4122(), $resultEmployeeJohnSecond->employee->id);
    }

    /**
     * @test
     */
    public function it_should_generate_report_with_employees_having_provided_last_name(FunctionalTester $I): void
    {
        // GIVEN
        $filteredLastname = 'Doe';

        // WHEN
        /** @var SalaryReportCollection $result */
        $result = $I->runQuery(new GetEmployeesSalaryReportQuery(filterLastname: $filteredLastname));

        // THEN
        $results = \iterator_to_array($result);
        $I->assertCount(3, $results);

        $resultEmployeeJohnFirst = \array_pop($results);
        $I->assertEquals($this->employeeJaneDoeHR->getId()->toRfc4122(), $resultEmployeeJohnFirst->employee->id);

        $resultEmployeeJohnSecond = \array_pop($results);
        $I->assertEquals($this->employeeJohnDoeHR->getId()->toRfc4122(), $resultEmployeeJohnSecond->employee->id);

        $resultEmployeeJohnSecond = \array_pop($results);
        $I->assertEquals($this->employeeJohnDoeQA->getId()->toRfc4122(), $resultEmployeeJohnSecond->employee->id);
    }

    /**
     * @test
     */
    public function it_should_generate_report_with_employees_being_in_provided_department(FunctionalTester $I): void
    {
        // GIVEN
        $departmentName = 'QA';

        // WHEN
        /** @var SalaryReportCollection $result */
        $result = $I->runQuery(new GetEmployeesSalaryReportQuery(filterDepartmentName: $departmentName));

        // THEN
        $results = \iterator_to_array($result);
        $I->assertCount(2, $results);

        $resultEmployeeJohnFirst = \array_pop($results);
        $I->assertEquals($this->employeeJohnDoeQA->getId()->toRfc4122(), $resultEmployeeJohnFirst->employee->id);

        $resultEmployeeJohnSecond = \array_pop($results);
        $I->assertEquals($this->employeeAliceWonderlandQA->getId()->toRfc4122(), $resultEmployeeJohnSecond->employee->id);
    }

    /**
     * @test
     */
    public function it_should_generate_report_with_all_filters(FunctionalTester $I): void
    {
        // GIVEN
        $firstName = 'John';
        $lastName = 'Doe';
        $departmentName = 'HR';

        // WHEN
        /** @var SalaryReportCollection $result */
        $result = $I->runQuery(
            new GetEmployeesSalaryReportQuery(
                filterFirstname: $firstName, filterLastname: $lastName, filterDepartmentName: $departmentName
            )
        );

        // THEN
        $results = \iterator_to_array($result);
        $I->assertCount(1, $results);

        $resultEmployeeJohnFirst = \array_pop($results);
        $I->assertEquals($this->employeeJohnDoeHR->getId()->toRfc4122(), $resultEmployeeJohnFirst->employee->id);
    }
}
