<?php

declare(strict_types=1);

namespace App\Tests\functional\App\Command;

use App\Command;
use App\Tests\FunctionalTester;
use Codeception\Example;
use Infrastructure\Department\Doctrine\Department;
use Payroll\Employee\Salary\SalaryAddition\SalaryAdditionType;

class PayrollDepartmentAddCommandCest
{
    /**
     * @dataProvider getNewDepartments
     * @test
     */
    public function it_should_create_new_department(FunctionalTester $I, Example $example): void
    {
        // GIVEN
        $departmentName = $example['name'];
        $departmentSalaryAddition = $example['salary_addition'];
        $expectedSalaryAdditionType = $example['salary_addition_type'];
        $expectedSalaryValue = $example['salary_addition_value'];

        // WHEN
        $output = $I->runSymfonyConsoleCommand('payroll:department:add', [
            'department-name'            => $departmentName,
            'department-salary-addition' => $departmentSalaryAddition,
        ]);

        // THEN
        $I->assertStringContainsString('Department has been created.', $output);
        $I->seeInRepository(Department::class, [
            'name'                => $departmentName,
            'salaryAdditionType'  => $expectedSalaryAdditionType,
            'salaryAdditionValue' => $expectedSalaryValue,
        ]);
    }

    public function getNewDepartments(): array
    {
        return [
            [
                'name'                  => 'New department with fixed salary addition.',
                'salary_addition'       => '42',
                'salary_addition_type'  => SalaryAdditionType::FIXED->value,
                'salary_addition_value' => 42_00, // 42.00
            ],
            [
                'name'                  => 'New department with percentage salary addition.',
                'salary_addition'       => '55%',
                'salary_addition_type'  => SalaryAdditionType::PERCENTAGE->value,
                'salary_addition_value' => 55, // 55%
            ],
        ];
    }

    /**
     * @test
     */
    public function it_should_not_allow_to_create_department_with_same_name_as_any_existing_one(FunctionalTester $I
    ): void {
        // GIVEN
        $departmentName = 'Taken name.';
        $expectedCommandExitCode = Command::FAILURE;

        $I->haveInRepository(Department::class, [
            'name' => $departmentName,
        ]);

        // WHEN
        $output = $I->runSymfonyConsoleCommand(command: 'payroll:department:add', parameters: [
            'department-name'            => $departmentName,
            'department-salary-addition' => '2500',
        ], expectedExitCode: $expectedCommandExitCode);

        // THEN
        $I->assertStringContainsString(\sprintf('Department with name "%s" already exists.', $departmentName), $output);
    }

    /**
     * @dataProvider getDepartmentNameValidationCases
     * @test
     */
    public function it_should_not_allow_to_create_department_if_name_do_not_pass_validation(FunctionalTester $I, Example $example): void
    {
        // GIVEN
        $departmentName = $example['name'];
        $expectedCommandExitCode = Command::FAILURE;

        // WHEN
        $output = $I->runSymfonyConsoleCommand(command: 'payroll:department:add', parameters: [
            'department-name'            => $departmentName,
            'department-salary-addition' => '2500',
        ], expectedExitCode: $expectedCommandExitCode);

        // THEN
        $I->assertStringContainsString($example['error'], $output);
    }

    public function getDepartmentNameValidationCases(): array
    {
        return [
            [
                'name'  => '', // empty name
                'error' => 'Department name cannot be empty.',
            ],
            [
                'name'  => \str_pad('', 251, 'a'), // name with 251 characters
                'error' => 'Department name cannot be longer than 250 characters.',
            ],
        ];
    }

    /**
     * @dataProvider getDepartmentSalaryAdditionValidationCases
     * @test
     */
    public function it_should_not_allow_to_create_department_if_salary_addition_do_not_pass_validation(FunctionalTester $I, Example $example): void
    {
        // GIVEN
        $departmentSalaryAddition = $example['salary_addition'];
        $expectedCommandExitCode = Command::FAILURE;

        // WHEN
        $output = $I->runSymfonyConsoleCommand(command: 'payroll:department:add', parameters: [
            'department-name'            => 'Test',
            'department-salary-addition' => $departmentSalaryAddition,
        ], expectedExitCode: $expectedCommandExitCode);

        // THEN
        $I->assertStringContainsString($example['error'], $output);
    }

    public function getDepartmentSalaryAdditionValidationCases(): array
    {
        return [
            [
                'salary_addition'  => 'ABC', // unsupported value
                'error' => 'Salary addition value "ABC" is not supported.',
            ],
            [
                'salary_addition'  => '10.9', // invalid money format
                'error' => 'Salary addition value "10.9" is not supported.',
            ],
            [
                'salary_addition'  => '101%', // too low percentage value
                'error' => 'Salary percentage value cannot be lower than 1% and higher than 100%.',
            ],
            [
                'salary_addition'  => '101%', // too high percentage value
                'error' => 'Salary percentage value cannot be lower than 1% and higher than 100%.',
            ],
        ];
    }
}
