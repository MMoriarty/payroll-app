<?php

declare(strict_types=1);

namespace App\Tests\functional\App\Command;

use App\Command;
use App\Tests\FunctionalTester;
use Codeception\Example;
use Infrastructure\Department\Doctrine\Department;
use Infrastructure\Employee\Doctrine\Employee;
use Payroll\Employee\Salary\SalaryAddition\SalaryAdditionType;
use Symfony\Component\Uid\Uuid;

class PayrollEmployeeAddCommandCest
{
    /**
     * @test
     */
    public function it_should_add_new_employee(FunctionalTester $I): void
    {
        // GIVEN
        $departmentAssignedToNewEmployee = 'Some test department';
        $employeeFirstName = 'John';
        $employeeLastName = 'Doe';

        $I->haveInRepository(Department::class, [
            'id'                  => Uuid::v4(),
            'name'                => $departmentAssignedToNewEmployee,
            'salaryAdditionType'  => SalaryAdditionType::PERCENTAGE->value,
            'salaryAdditionValue' => 25,
        ]);

        // WHEN
        $output = $I->runSymfonyConsoleCommand('payroll:employee:add', [
            'first-name'      => $employeeFirstName,
            'last-name'       => $employeeLastName,
            'department-name' => $departmentAssignedToNewEmployee,
            'salary'          => '1250',
        ]);

        // THEN
        $I->assertStringContainsString('New employee has been added.', $output);
        $I->seeInRepository(Employee::class, [
            'firstName' => $employeeFirstName,
            'lastName'  => $employeeLastName,
        ]);
    }

    /**
     * @test
     */
    public function it_should_add_new_employee_with_custom_employment_date(FunctionalTester $I): void
    {
        // GIVEN
        $departmentAssignedToNewEmployee = 'Some test department';
        $employeeFirstName = 'Jane';
        $employeeLastName = 'Doe';
        $customEmploymentDate = '2012-12-21';

        $I->haveInRepository(Department::class, [
            'id'                  => Uuid::v4(),
            'name'                => $departmentAssignedToNewEmployee,
            'salaryAdditionType'  => SalaryAdditionType::PERCENTAGE->value,
            'salaryAdditionValue' => 25,
        ]);

        // WHEN
        $output = $I->runSymfonyConsoleCommand('payroll:employee:add', [
            'first-name'        => $employeeFirstName,
            'last-name'         => $employeeLastName,
            'department-name'   => $departmentAssignedToNewEmployee,
            'salary'            => '1250',
            '--employment-date' => $customEmploymentDate,
        ]);

        // THEN
        $I->assertStringContainsString('New employee has been added.', $output);

        $employee = $I->grabEntityFromRepository(
            Employee::class,
            ['firstName' => $employeeFirstName, 'lastName' => $employeeLastName]
        );
        $I->assertEquals($customEmploymentDate, $employee->getEmploymentDate()->format('Y-m-d'));
    }

    /**
     * @test
     */
    public function it_should_fail_if_department_does_not_exist(FunctionalTester $I): void
    {
        // GIVEN
        $departmentAssignedToNewEmployee = 'Some test department';
        $employeeFirstName = 'Jane';
        $employeeLastName = 'Doe';
        $customEmploymentDate = '2012-12-21';

        // WHEN
        $output = $I->runSymfonyConsoleCommand(command: 'payroll:employee:add', parameters: [
            'first-name'        => $employeeFirstName,
            'last-name'         => $employeeLastName,
            'department-name'   => $departmentAssignedToNewEmployee,
            'salary'            => '1250',
            '--employment-date' => $customEmploymentDate,
        ], expectedExitCode: Command::FAILURE);

        // THEN
        $I->assertStringContainsString('Department with name "Some test department" does not exists.', $output);
    }

    /**
     * @dataProvider getValidationCases
     * @test
     */
    public function it_should_fail_when_validation_do_not_pass_one_of_values(
        FunctionalTester $I,
        Example $example
    ): void {
        // GIVEN
        $departmentAssignedToNewEmployee = 'Some test department';
        $employeeFirstName = $example['first_name'];
        $employeeLastName = $example['last_name'];
        $customEmploymentDate = $example['employment_date'];
        $employeeSalary = $example['salary'];
        $expectedErrorMessage = $example['error'];

        $I->haveInRepository(Department::class, [
            'id'                  => Uuid::v4(),
            'name'                => $departmentAssignedToNewEmployee,
            'salaryAdditionType'  => SalaryAdditionType::PERCENTAGE->value,
            'salaryAdditionValue' => 25,
        ]);

        // WHEN
        $output = $I->runSymfonyConsoleCommand(command: 'payroll:employee:add', parameters: [
            'first-name'        => $employeeFirstName,
            'last-name'         => $employeeLastName,
            'department-name'   => $departmentAssignedToNewEmployee,
            'salary'            => $employeeSalary,
            '--employment-date' => $customEmploymentDate,
        ], expectedExitCode: Command::FAILURE);

        // THEN
        $I->assertStringContainsString($expectedErrorMessage, $output);
    }

    public function getValidationCases(): array
    {
        return [
            [
                'first_name'      => '', // empty first name
                'last_name'       => 'Doe',
                'employment_date' => '2012-12-21',
                'salary'          => '1250.00',
                'error'           => 'First name cannot be empty.',
            ],
            [
                'first_name'      => 'Jane',
                'last_name'       => '', // empty last name
                'employment_date' => '2012-12-21',
                'salary'          => '1250.00',
                'error'           => 'Last name cannot be empty.',
            ],
            [
                'first_name'      => 'Jane',
                'last_name'       => 'Doe',
                'employment_date' => '55-77-ABC', // invalid date
                'salary'          => '1250.00',
                'error'           => 'Expected date format is "Y-m-d"',
            ],
            [
                'first_name'      => 'Jane',
                'last_name'       => 'Doe',
                'employment_date' => '2012-12-21',
                'salary'          => 'ABC', // invalid salary
                'error'           => 'Invalid salary value "ABC".',
            ],
            [
                'first_name'      => 'Jane',
                'last_name'       => 'Doe',
                'employment_date' => '2012-12-21',
                'salary'          => '0', // too low salary
                'error'           => 'Salary cannot be equal to 0.',
            ],
        ];
    }
}
