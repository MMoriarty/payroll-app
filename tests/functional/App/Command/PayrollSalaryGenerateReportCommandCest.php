<?php

declare(strict_types=1);

namespace App\Tests\functional\App\Command;

use App\Tests\FunctionalTester;
use Payroll\Employee\Salary\SalaryAddition\SalaryAdditionType;

class PayrollSalaryGenerateReportCommandCest
{
    /**
     * @test
     */
    public function it_should_show_report_with_employees_salaries(FunctionalTester $I): void
    {
        // GIVEN
        $departmentQA = $I->haveDepartment('QA', SalaryAdditionType::FIXED, 250_00);
        $departmentHR = $I->haveDepartment('HR', SalaryAdditionType::PERCENTAGE, 10);
        $I->haveEmployee($departmentQA, 'John', 'Doe', 4_000_00, new \DateTime('-1 day'));
        $I->haveEmployee($departmentHR, 'Sherlock', 'Holmes', 7_500_00, new \DateTime('-2 day'));
        $I->haveEmployee($departmentHR, 'Alice', 'Wonderland', 7_250_00, new \DateTime('-3 day'));

        // WHEN
        $output = $I->runSymfonyConsoleCommand('payroll:salary:generate-report');

        // THEN
        $I->assertStringContainsString('│ Alice      │ Wonderland │ HR         │ 7250.00     │ 725.00          │ Percentage    │ 7975.00      │', $output);
        $I->assertStringContainsString('│ Sherlock   │ Holmes     │ HR         │ 7500.00     │ 750.00          │ Percentage    │ 8250.00      │', $output);
        $I->assertStringContainsString('│ John       │ Doe        │ QA         │ 4000.00     │ 0.00            │ Fixed         │ 4000.00      │', $output);
    }
}
