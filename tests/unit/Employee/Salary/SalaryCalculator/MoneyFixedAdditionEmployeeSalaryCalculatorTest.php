<?php

declare(strict_types=1);

namespace App\Tests\unit\Employee\Salary\SalaryCalculator;

use App\Clock\Clock;
use Codeception\Test\Unit;
use Infrastructure\Employee\Salary\MoneyCurrencyFactory;
use Infrastructure\Employee\Salary\SalaryCalculator\MoneyFixedAdditionEmployeeSalaryCalculator;
use Payroll\Department\Department;
use Payroll\Employee\Employee;
use Payroll\Employee\Salary\Exception\SalaryCalculationModeNotSupportedException;
use Payroll\Employee\Salary\SalaryAddition\SalaryAddition;
use Payroll\Employee\Salary\SalaryAddition\SalaryAdditionType;

class MoneyFixedAdditionEmployeeSalaryCalculatorTest extends Unit
{
    /**
     * @dataProvider getCalculationDataset
     * @test
     * @throws SalaryCalculationModeNotSupportedException
     */
    public function it_should_calculate_employee_salary_for_fixed_addition_type(
        int $baseSalary,
        int $salaryAddition,
        \DateTime $currentDate,
        \DateTime $employmentDate,
        int $expectedTotalSalary,
        int $expectedTotalAddition
    ): void {
        // GIVEN
        $clock = $this->createMock(Clock::class);
        $clock->expects($this->once())->method('now')->willReturn($currentDate);

        $department = new Department('QA', new SalaryAddition(SalaryAdditionType::FIXED, $salaryAddition));
        $employee = new Employee('John', 'Doe', $baseSalary, $department, $employmentDate);

        // WHEN
        $calculator = $this->createMoneyFixedAdditionEmployeeSalaryCalculator($clock);
        $supported = $calculator->supports($employee);
        $result = $calculator->calculateForEmployee($employee);

        // THEN
        $this->assertTrue($supported);
        $this->assertEquals($expectedTotalAddition, $result->salaryAddition);
        $this->assertEquals($expectedTotalSalary, $result->totalSalary);
    }

    public function getCalculationDataset(): array
    {
        return [
            [
                'baseSalary'            => 5_000_00, // 5'000.00
                'salaryAddition'        => 250_00, // 250.00
                'currentDate'           => \DateTime::createFromFormat('Y-m-d', '2021-12-18'),
                'employmentDate'        => \DateTime::createFromFormat('Y-m-d', '2016-12-01'), // 5 years ago
                'expectedTotalSalary'   => 6_250_00, // 6'250.00
                'expectedTotalAddition' => 1_250_00, // 1'250.00
            ],
            [
                'baseSalary'            => 4_500_00, // 4'500.00
                'salaryAddition'        => 300_00, // 300.00
                'currentDate'           => \DateTime::createFromFormat('Y-m-d', '2021-11-30'),
                'employmentDate'        => \DateTime::createFromFormat('Y-m-d', '2016-12-05'), // 4 years 11 months ago
                'expectedTotalSalary'   => 5_700_00, // 5'700.00
                'expectedTotalAddition' => 1_200_00, // 1'250.00
            ],
            [
                'baseSalary'            => 10_000_00, // 10'000.00
                'salaryAddition'        => 100_00, // 250.00
                'currentDate'           => \DateTime::createFromFormat('Y-m-d', '2021-12-18'),
                'employmentDate'        => \DateTime::createFromFormat('Y-m-d', '1996-12-18'), // 25 years ago
                'expectedTotalSalary'   => 11_000_00, // 11'000.00
                'expectedTotalAddition' => 1000_00, // 1'000.00
            ],
            [
                'baseSalary'            => 2_000_00, // 2'000.00
                'salaryAddition'        => 50_00, // 50.00
                'currentDate'           => \DateTime::createFromFormat('Y-m-d', '2021-12-18'),
                'employmentDate'        => \DateTime::createFromFormat('Y-m-d', '2021-12-15'), // 3 days ago
                'expectedTotalSalary'   => 2_000_00, // 2'000.00
                'expectedTotalAddition' => 0, // 0.00
            ],
            [
                'baseSalary'            => 1_234_00, // 1'234.00
                'salaryAddition'        => 74_00, // 74.00
                'currentDate'           => \DateTime::createFromFormat('Y-m-d', '2021-12-18'),
                'employmentDate'        => \DateTime::createFromFormat('Y-m-d', '2015-12-18'), // 6 years ago
                'expectedTotalSalary'   => 1_678_00, // 1'678.00
                'expectedTotalAddition' => 444_00, // 444.00
            ],
            [
                'baseSalary'            => 700_00, // 700.00
                'salaryAddition'        => 5_30, // 5.30
                'currentDate'           => \DateTime::createFromFormat('Y-m-d', '2021-12-18'),
                'employmentDate'        => \DateTime::createFromFormat('Y-m-d', '2019-12-18'), // 2 years ago
                'expectedTotalSalary'   => 710_60, // 710.60
                'expectedTotalAddition' => 10_60, // 10.60
            ],
        ];
    }

    private function createMoneyFixedAdditionEmployeeSalaryCalculator(Clock $clock
    ): MoneyFixedAdditionEmployeeSalaryCalculator {
        return new MoneyFixedAdditionEmployeeSalaryCalculator(new MoneyCurrencyFactory('PLN'), $clock);
    }
}
