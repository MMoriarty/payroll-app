<?php

declare(strict_types=1);

namespace App\Tests\unit\Employee\Salary\SalaryCalculator;

use Codeception\Test\Unit;
use Infrastructure\Employee\Salary\MoneyCurrencyFactory;
use Infrastructure\Employee\Salary\SalaryCalculator\MoneyPercentageAdditionEmployeeSalaryCalculator;
use Payroll\Department\Department;
use Payroll\Employee\Employee;
use Payroll\Employee\Salary\Exception\SalaryCalculationModeNotSupportedException;
use Payroll\Employee\Salary\SalaryAddition\SalaryAddition;
use Payroll\Employee\Salary\SalaryAddition\SalaryAdditionType;

class MoneyPercentageAdditionEmployeeSalaryCalculatorTest extends Unit
{
    /**
     * @dataProvider getCalculationDataset
     * @test
     * @throws SalaryCalculationModeNotSupportedException
     */
    public function it_should_calculate_employee_salary_for_percentage_addition_type(
        int $baseSalary,
        int $salaryAddition,
        int $expectedTotalSalary,
        int $expectedTotalAddition
    ): void {
        // GIVEN
        $department = new Department('QA', new SalaryAddition(SalaryAdditionType::PERCENTAGE, $salaryAddition));
        $employee = new Employee('John', 'Doe', $baseSalary, $department, new \DateTime());

        // WHEN
        $calculator = $this->createMoneyPercentageAdditionEmployeeSalaryCalculator();
        $supported = $calculator->supports($employee);
        $result = $calculator->calculateForEmployee($employee);

        // THEN
        $this->assertTrue($supported);
        $this->assertEquals($expectedTotalAddition, $result->salaryAddition);
        $this->assertEquals($expectedTotalSalary, $result->totalSalary);
    }

    public function getCalculationDataset(): array
    {
        return [
            [
                'baseSalary'            => 1_000_00, // 1'000.00
                'salaryAddition'        => 10, // 10%
                'expectedTotalSalary'   => 1_100_00, // 1'100.00
                'expectedTotalAddition' => 100_00, // 100.00
            ],
            [
                'baseSalary'            => 2_000_00, // 2'000.00
                'salaryAddition'        => 100, // 100%
                'expectedTotalSalary'   => 4_000_00, // 4'000.00
                'expectedTotalAddition' => 2_000_00, // 2'000.00
            ],
            [
                'baseSalary'            => 1_550_00, // 1'550.00
                'salaryAddition'        => 33, // 33%
                'expectedTotalSalary'   => 2_061_50, // 2'061.50
                'expectedTotalAddition' => 511_50, // 511.50
            ],
            [
                'baseSalary'            => 9_000_00, // 9'000.00
                'salaryAddition'        => 0, // 0%
                'expectedTotalSalary'   => 9_000_00, // 9'000.00
                'expectedTotalAddition' => 0, // 0.00
            ],
        ];
    }

    private function createMoneyPercentageAdditionEmployeeSalaryCalculator(): MoneyPercentageAdditionEmployeeSalaryCalculator
    {
        return new MoneyPercentageAdditionEmployeeSalaryCalculator(new MoneyCurrencyFactory('PLN'));
    }
}
