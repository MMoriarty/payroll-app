<?php

declare(strict_types=1);

namespace App\Tests\unit\Employee\Salary\SalaryAddition\Extractor;

use Codeception\Test\Unit;
use Infrastructure\Employee\Salary\SalaryAddition\Extractor\FixedSalaryAdditionExtractor;
use Payroll\Employee\Salary\SalaryAddition\SalaryAdditionType;
use Payroll\Employee\Salary\SalaryParser;

class FixedSalaryAdditionExtractorTest extends Unit
{
    /**
     * @dataProvider getPossibleFixedValues
     * @test
     */
    public function it_should_extract_fixed_salary_from_provided_value(string $value, int $result): void
    {
        // GIVEN
        $salaryParser = $this->createMock(SalaryParser::class);
        $salaryParser->expects($this->once())->method('parse')->with($value)->willReturn($result);

        // WHEN
        $extractor = new FixedSalaryAdditionExtractor($salaryParser);
        $supported = $extractor->supports($value);
        $salaryAddition = $extractor->extractFrom($value);

        // THEN
        $this->assertTrue($supported);
        $this->assertEquals($result, $salaryAddition->value);
        $this->assertEquals(SalaryAdditionType::FIXED, $salaryAddition->type);
    }

    public function getPossibleFixedValues(): array
    {
        return [
            ['12.50', 1250],
            ['50', 5000],
            ['0.33', 33],
        ];
    }
}
