<?php

declare(strict_types=1);

namespace App\Tests\unit\Employee\Salary\SalaryAddition\Extractor;

use Codeception\Test\Unit;
use Infrastructure\Employee\Salary\SalaryAddition\Extractor\PercentageSalaryAdditionExtractor;
use Payroll\Employee\Salary\SalaryAddition\SalaryAdditionType;

class PercentageSalaryAdditionExtractorTest extends Unit
{
    /**
     * @dataProvider getPossiblePercentageValues
     * @test
     */
    public function it_should_extract_percentage_salary_from_provided_value(string $percentage, int $expectedResult): void
    {
        // WHEN
        $extractor = new PercentageSalaryAdditionExtractor();
        $supported = $extractor->supports($percentage);
        $result = $extractor->extractFrom($percentage);

        // THEN
        $this->assertTrue($supported);
        $this->assertEquals($expectedResult, $result->value);
        $this->assertEquals(SalaryAdditionType::PERCENTAGE, $result->type);
    }

    public function getPossiblePercentageValues(): array
    {
        return [
            ['100%', 100],
            ['50%', 50],
            ['2%', 2],
        ];
    }
}
