<?php

declare(strict_types=1);

namespace App\Tests\Helper;

use App\Bus\QueryBus;
use Codeception\Module;
use Symfony\Component\DependencyInjection\ContainerInterface;

class Functional extends Module
{
    public function runQuery(object $query)
    {
        return $this->getContainer()->get(QueryBus::class)->handle($query);
    }

    private function getContainer(): ContainerInterface
    {
        /** @noinspection PhpPossiblePolymorphicInvocationInspection */
        /** @noinspection PhpUnhandledExceptionInspection */
        return $this->getModule('Symfony')->_getContainer();
    }
}
