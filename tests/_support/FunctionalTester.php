<?php

declare(strict_types=1);

namespace App\Tests;

use Codeception\Actor;
use Codeception\Lib\Friend;
use Infrastructure\Department\Doctrine\Department;
use Infrastructure\Employee\Doctrine\Employee;
use Payroll\Employee\Salary\SalaryAddition\SalaryAdditionType;

/**
 * Inherited Methods
 * @method void wantToTest($text)
 * @method void wantTo($text)
 * @method void execute($callable)
 * @method void expectTo($prediction)
 * @method void expect($prediction)
 * @method void amGoingTo($argumentation)
 * @method void am($role)
 * @method void lookForwardTo($achieveValue)
 * @method void comment($description)
 * @method Friend haveFriend($name, $actorClass = null)
 *
 * @SuppressWarnings(PHPMD)
 */
class FunctionalTester extends Actor
{
    use _generated\FunctionalTesterActions;

    public function haveDepartment(
        string $name,
        SalaryAdditionType $salaryAdditionType,
        int $salaryAdditionValue
    ): Department {
        $departmentId = $this->haveInRepository(Department::class, [
            'name'                => $name,
            'salaryAdditionType'  => $salaryAdditionType->value,
            'salaryAdditionValue' => $salaryAdditionValue,
        ]);

        return $this->grabEntityFromRepository(Department::class, [
            'id' => $departmentId->toBinary(),
        ]);
    }

    public function haveEmployee(
        Department $department,
        string $firstName,
        string $lastName,
        int $salary = null,
        \DateTime $employmentDate = null
    ): Employee {
        $employeeId = $this->haveInRepository(Employee::class, [
            'firstName'      => $firstName,
            'lastName'       => $lastName,
            'salary'         => $salary ?? \random_int(1_000_00, 10_000_00),
            'department'     => $department,
            'employmentDate' => $employmentDate ?? new \DateTime(),
        ]);

        return $this->grabEntityFromRepository(Employee::class, ['id' => $employeeId->toBinary()]);
    }
}
