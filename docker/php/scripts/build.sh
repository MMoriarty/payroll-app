#!/usr/bin/env sh

apk update
apk add --no-cache ${PHPIZE_DEPS} libzip-dev icu-dev

docker-php-ext-install -j$(nproc) pdo_mysql intl bcmath
