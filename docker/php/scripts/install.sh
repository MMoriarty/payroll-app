#!/usr/bin/env sh

apk update
apk add --no-cache icu

docker-php-ext-enable pdo_mysql
docker-php-ext-enable intl
docker-php-ext-enable bcmath

ln -s /source/bin/console /bin/app
ln -s /source/vendor/bin/codecept /bin/codecept
